<?php
//定义CanPHP框架目录
date_default_timezone_set('Asia/Shanghai');
define('CP_PATH', dirname(__file__) . '/include/'); //指定内核目录
require(dirname(__FILE__).'/config.php');//加载配置
require(CP_PATH.'core/cpApp.class.php');//加载应用控制类

$app=new cpApp($config);//实例化单一入口应用控制类
//执行项目
$app->run();
?>