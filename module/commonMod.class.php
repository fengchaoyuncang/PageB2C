<?php
//公共类
class commonMod {
    public $model; //数据库模型对象
    public $tpl; //模板对象
    public $config; //全局配置
    static $global; //静态变量，用来实现单例模式

    public function __construct() {
        date_default_timezone_set('Asia/Shanghai');  //配置系统的时区
        header("Centent-Type:text/html;charset=UTF-8");   //设置系统编码格式
        session_start(); //开启session
        //如果还没有安装，则调整到安装页面
         if (!file_exists('data/install.lock')) {
            $this->redirect(__ROOT__ . '/install/index.php');
         }
        //参数配置
        if (!isset(self::$global['config'])) {
            global $config;
            self::$global['config'] = $config;
        }
        $this->config = self::$global['config']; //配置
        //数据库模型初始化
        if (!isset(self::$global['model'])) {
            self::$global['model'] = new cpModel($this->config); //实例化数据库模型类
        }
        $this->model = self::$global['model']; //数据库模型对象
        //模板初始化
        if (!isset(self::$global['tpl'])) {
            self::$global['tpl'] = new cpTemplate($this->config); //实例化模板类		
        }
        $this->tpl = self::$global['tpl']; //模板类对象
    }
    
    //模板变量解析
    protected function assign($name, $value) {
        return $this->tpl->assign($name, $value);
    }

    //模板输出
    protected function display($tpl = '') {
        return $this->tpl->display($tpl);
    }

    //直接跳转
    protected function redirect($url) {
        header('location:' . $url, false, 301);
        exit;
    }
    //文件上传
    protected function imgupload($upload_dir) {    //imgupload是文件上传的方法名
        $upload = new UploadFile();  //实例化UploadFile();
        //以下是一些上传文件的参数设置
        //设置上传文件大小
        $upload->maxSize = 1024 * 1024 * 2;  //限制上传文件最大2M
        //设置上传文件类型
        $upload->allowExts = explode(',', 'jpg,gif,png,bmp');  //限制上传的文件的类型，可根据您要上传的文件类型进行修改
        //设置附件上传目录
        $upload->savePath = 'upload/' . $upload_dir . "/"; //设置文件上传目录的路径
        $upload->saveRule = cp_uniqid();   //设置文件上传保存的文件名，为空则以本地的文件名原名保存
        //以下没什么好说的，就是判断上传文件的状态
        if ($upload->upload()) {
            //取得成功上传的文件信息
            return $upload->getUploadFileInfo();
        }
    }
    //判断会员积分等级
    //class参数1时是等级名称，空是等级折扣
    protected function userinfo($userid,$class = null){
        $grade = $this->model->table('grade')->field('vip,rebate,name')->where()->select();
        $fid = in($userid); //读取用户
        $condition['id'] = intval($fid);
        $user = $this->model->table('user')->field('allvip')->where($condition)->find();
        foreach ($grade as $value) {
            if ($user['allvip'] <= $value['vip']) {
                $array[] = $value;
            }
        }
        $array[] = sort($array);
        if($class){
            return $array[0]['name'];
        }else{
            return $array[0]['rebate'];
        }
    }
    //登录后用户的COOKIE信息还原
    protected function uvip($class){
        switch ($class){
            case 1:
                return cp_decode($_COOKIE['VipID'],md5(Easyes));
                break;
           case 2:
                return cp_decode($_COOKIE['VipUser'],md5(Easyes));
                break;
           case 3:
                return cp_decode($_COOKIE['VipName'],md5(Easyes));
                break;
           default:
               return 0;
        }
    }
}