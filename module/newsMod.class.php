<?php
class newsMod extends commonMod {
    public function __construct() {
        parent::__construct();
    }
    /*     * *************************************
     * 新闻管理方法集
     * ***************************************** */
    //方法:新闻分类列表
    //参数:无
    public function index() {
        $cat_id = intval(in($_GET[0])); //分类ID
        //分类
        $condition['pid'] = $cat_id;
        $class = $this->model->field('id,pid,name')->table('newsclass')->where($condition)->select();
        $this->assign('class', $class);
        unset($condition);
        if (!empty($cat_id)) {
            $where = ' and classid in(' . $cat_id . $this->AllChildClass($cat_id) . ')';
            $url = __URL__ . '/index-' . $cat_id . '-{page}.html';
        }
        //分页开始
        $url = __URL__ . '/index-0-{page}.html';
        $listRows = 20; //每页显示的信息条数 
        $page = new Page();
        $cur_page = $page->getCurPage($url);
        $limit_start = ($cur_page - 1) * $listRows;
        $limit = $limit_start . ',' . $listRows;
        //获取总行数
        $allcount = $this->model->table('news')->where('1' . $where)->count();
        $this->assign('allcount', $allcount);
        //读取数据开始
        $info = $this->model->table('news')->where('1' . $where)->limit($limit)->select();
        $this->assign('page', $page->show($url, $allcount, $listRows, 10, 4));
        $this->assign('info', $info);
        $this->assign('path', $this->flpath($cat_id, '/index'));
        $this->display('news/index');   
    }
    //阅读查看
    public function read(){
        $fid = in($_GET[0]); //读取用户
        $condition['id'] = intval($fid);
        $title = $this->model->table('news')->where($condition)->find();
        $this->assign('title', $title);  
        //调用评论
        $concomment['pid']     = $fid;
        $concomment['classid'] = 2;
        $concomment['open']    = 1;
        $info = $this->model->table('comment')->where($concomment)->order('id desc')->select();
        $this->assign('info', $info); 
        $this->display('news/read');
        
    }
    //评论提交
    public function commentpost(){
        $action = $_POST['action'];
        if ($action == "post") {//评论提交
            $msg = Check::rule(
                    array(check::must($_POST['pid']), '评论主ID不能为空'),
                    array(check::must($_POST['content']), '对不起！评论内容没有填写')
            );
            if ($msg !== true) {
                json(0,$msg);
            }
            if (!isset($_COOKIE['VipID'])) {//判断是否会员
                json(3, '只有会员才能评论，请登录',__APP__.'/index/login?url='.__URL__.'/read-'.$_POST['pid'].'html');  
            }
            if ($_COOKIE['commentid'] == $_POST['pid']) {
                json(0, '同一主题10分钟内内禁止重复评论'); 
            }
            $_POST['uptime']  = time();
            $_POST['classid'] = 2;
            $_POST['userid']  = $this->uvip(1);
            $_POST['username']= $this->uvip(3);
            $data = postinput($_POST); 
            $result = $this->model->table('comment')->data($data)->insert();
            if($result){
                $vip = $this->model->table('sys')->field('gold')->where('id = 5')->find();
                $id = $this->uvip(1);
                $sql = "UPDATE ".$this->model->pre."user SET allvip = allvip+".$vip['gold'].",vip = vip+".$vip['gold']." WHERE id=".$id;
                $this->model->query($sql); 
                setcookie("commentid",$_POST['pid'], time() + 60,'/');
                json(3, '留言成功!管理员审核后即可显示',__URL__.'/read-'.$_POST['pid'].'.html');
            }else{
                json(0, '对不起！数据提交失败请刷新重新提交');
            }  
        }
    }
    //方法：递归搜当前目录下所有的下级目录（无限层型目录）
    //参数：栏目ID
    protected function AllChildClass($pid = 0) {
        $pid = intval($pid);
        $sql = $this->getClass($pid);
        foreach ($sql as $pid) {
            $str .= "," . $pid['id'];
        }
        return $str;
    }
    //方法:获取分类树， 获取ID包含所有分类结构树
    //参数:父栏目ID
    public function getClass($id=0) {
        //查询分类信息
        $data = $this->model->field('id,pid,name')->table('newsclass')->select();
        $cat = new Category(array('id', 'pid', 'name', 'cname')); //初始化无限分类
        return $cat->getTree($data, $id); //获取分类数据树结构
    }
    //方法:当前分类的位置
    //参数：pid栏目ID,action当前位置栏目连接页面
    protected function flpath($pid = 0, $action) {
        $condition['id'] = intval($pid);
        $pname = $this->model->table('newsclass')->field('id,pid,name')->where($condition)->find();
        if ($pname) {
            $Str = $this->flpath($pname['pid'], $action);
            $Str .= '<a href="' . __URL__ . $action . '-' . $pname['id'] . '.html' . '">' . $pname['name'] . '</a> >> ';
        }
        return $Str;
    }
}