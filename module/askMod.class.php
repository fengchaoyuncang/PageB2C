<?php
class askMod extends commonMod {
    public function __construct() {
        parent::__construct();
    }
    public function index() {
        $fid = in($_GET[0]); //读取用户
        if(!$fid){
            Error::show('参数错误', 0);
        }
        $condition['id'] = intval($fid);
        $title = $this->model->table('askclass')->where($condition)->find();
        $this->assign('title', $title);
        $conask['classid'] = intval($fid);
        $info = $this->model->table('ask')->where($conask)->select();
        $this->assign('info', $info);
        $convate['classid'] = intval($fid);
        $vate = $this->model->table('vate')->field('id,classid,askid,name')->where($convate)->select();
        $this->assign('vate', $vate);     
        $this->display('ask/index');  
    }
    //提交调查
    public function postask(){
        $action = $_POST['action'];
        if ($action == 'post') {
            if (!isset($_COOKIE['VipID'])) {
                json(3, '对不起！您还不是会员不能参加调查',__APP__.'/index/login?url='.__URL__.'/index-'.$_POST['classid'].'.html');
            }
            if(!$_POST['classid']){
               json(0, '没有选择调查主题'); 
            }
            if ($_COOKIE['voteid'] == $_POST['classid']) {
                json(0, '同一主题10分钟内内禁止重复提交'); 
            }
            //开始入库
            $data['classid'] = in($_POST['classid']);
            $data['userid']  = $this->uvip(1);
            $data['uname']   = $this->uvip(3);
            //判断
            $fid = in($_POST['fid']);
            foreach($fid as $value){
                $op = in($_POST['option'.$value]);
                if(!is_array($op)){
                    json(1, '该题为必选题，请参与投票.',0,'option'.$value);
                }
                foreach($op as $vo){
                    $vateid = intval($vo);
                    $vatesql = "UPDATE ".$this->model->pre."vate SET vatecount=vatecount+1 WHERE id=".$vateid;
                    $result = $this->model->query($vatesql); 
                }
            }
            if($result){
                $vip = $this->model->table('askclass')->field('vip')->where('id = '.$data['classid'])->find();
                $id  = $this->uvip(1);
                $sql = "UPDATE ".$this->model->pre."user SET allvip = allvip+".$vip['vip'].",vip = vip+".$vip['vip']." WHERE id=".$id;
                $this->model->query($sql); 
                setcookie("voteid",$data['classid'], time() + 600,'/');
                json(3, '谢谢参与投票调查！',__APP__."/mycenter");
            }
        }
    }
}