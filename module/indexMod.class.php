<?php
class indexMod extends commonMod {
    public function __construct() {
        parent::__construct();
    }
    public function index() {
        //$this->redirect(__URL__.'/reg.html');
        $this->display('index/reg');
    }
    //完整注册
    public function reg() {
        $action = $_POST['action'];
        if ($action == 'post') {
            $msg = Check::rule(
                    array(check::must($_POST['username']), '用户名没有填写'),
                    array(check::must($_POST['password']), '密码没有填写'),
                    array(check::must($_POST['passcheck']), '确认密码没有填写'), 
                    array(check::same($_POST['password'], $_POST['passcheck']), '两次密码输入不相同'),
                    array(check::chackone($_POST['email'], $_POST['iphone']), 'Email和手机号最少要填一项'),
                    array(check::email($_POST['email']), '信箱输入错误'),
                    array(check::mobile($_POST['iphone']), '手机号输入错误'),
                    array(check::must($_POST['checkcode']), '验证码没有填写'),
                    array(check::same($_POST['checkcode'], $_SESSION['verify']), '验证码输入错误')
            );
            if ($msg !== true) {
                json(0,$msg);
            }
            //判断是否相同用户
            $username = in($_POST['username']);
            $email    = in($_POST['email']);
            $iphone   = in($_POST['iphone']);
            $condition = "username = '$username' or email = '$email' or iphone ='$iphone'";
            $info = $this->model->table('user')->where($condition)->find();
            if ($info) {
                if ($username == $info['username']) {
                    json(0, '相同的'. $sername. '已存在');
                }
                if ($email == $info['email']) {
                    json(0, '相同的'.$email.'已存在');
                }
                if ($iphone == $info['iphone']) {
                    json(0, '相同的' . $iphone . '已存在');
                }
            }
            //获取积分
            $convip['id'] = 1;
            $vip = $this->model->table('sys')->field('gold')->where($convip)->find();
            $_POST['allvip']= $vip['gold'];
            $_POST['vip'] = $vip['gold'];
            //入库
            if ($_POST['password'])
                $_POST['password'] = md5($_POST['password']);
            unset($_POST['passcheck']);
            unset($_POST['checkcode']);
            $_POST['regtime'] = time();
            $data = postinput($_POST);
            $result = $this->model->table('user')->data($data)->insert();
            if($result){
                //邀请赠送
                if($_COOKIE['youme']){
                    $youmevip = $this->model->table('sys')->field('gold')->where('id = 4')->find();
                    $youmeid = cp_decode($_COOKIE['youme']);
                    $sql = "UPDATE ".$this->model->pre."user SET allvip = allvip+".$youmevip['gold'].",vip = vip+".$youmevip['gold']." WHERE id=".$youmeid;
                    $this->model->query($sql); 
                    setcookie("youme", "", time() - 3600, '/');
                }
                //自动登录
                $loginid['id'] = mysql_insert_id();
                $user = $this->model->table('user')->field('id,username,name')->where($loginid)->find();
                setcookie("VipID", cp_encode($user['id'],md5('Easyes')), time() + 3600,'/');
                setcookie("VipUser", cp_encode($user['username'],md5('Easyes')), time() + 3600,'/');
                setcookie("VipName", cp_encode($user['name'],md5('Easyes')), time() + 3600,'/');
            //成功
                json(3, '注册成功',__APP__.'/mycenter');  
            }else{
                json(0, '注册失败！请重新确认注册');
            }
        }else{
            $youme = in($_GET['youme']);
            if($youme){
               setcookie("youme", $youme, time() + 600,'/');
            }
            echo cp_decode($_COOKIE['youme']);
            $this->display('index/reg');
        }
    }
    //简单注册
    public function freg() {
        $action = $_POST['action'];
        if ($action == 'post') {
            $msg = Check::rule(
                    array(check::must($_POST['username']), '用户名没有填写'),
                    array(check::must($_POST['password']), '密码没有填写'),
                    array(check::must($_POST['passcheck']), '确认密码没有填写'), 
                    array(check::same($_POST['password'], $_POST['passcheck']), '两次密码输入不相同'),
                    array(check::must($_POST['checkcode']), '验证码没有填写'),
                    array(check::same($_POST['checkcode'], $_SESSION['verify']), '验证码输入错误')
            );
            if ($msg !== true) {
                json(0,$msg);
            }
            //判断是否相同用户
            $username = in($_POST['username']);
            $condition['username'] = $username;
            $info = $this->model->table('user')->where($condition)->find();
            if ($info) {
                if ($username == $info['username']) {
                    json(0, '相同的'. $username. '已存在');
                }
            }
            $vip = $this->model->table('sys')->field('gold')->where('id = 1')->find(); //获取积分
            $_POST['allvip']= $vip['gold'];
            $_POST['vip'] = $vip['gold'];
            //入库
            if ($_POST['password'])
                $_POST['password'] = md5($_POST['password']);
            unset($_POST['passcheck']);
            unset($_POST['checkcode']);
            $_POST['regtime'] = time();
            $data = postinput($_POST);
            $data['ip'] = get_client_ip();
            $data['uptime'] = $_SERVER['REQUEST_TIME'];
            $result = $this->model->table('user')->data($data)->insert();
            if($result){
                //邀请赠送
                if($_COOKIE['youme']){
                    $youmevip = $this->model->table('sys')->field('gold')->where('id = 4')->find();
                    $youmeid = cp_decode($_COOKIE['youme']);
                    $sql = "UPDATE ".$this->model->pre."user SET allvip = allvip+".$youmevip['gold'].",vip = vip+".$youmevip['gold']." WHERE id=".$youmeid;
                    $this->model->query($sql); 
                    setcookie("youme", "", time() - 3600, '/');
                }
                //自动登录
                $loginid['id'] = mysql_insert_id();
                $user = $this->model->table('user')->field('id,username,name')->where($loginid)->find();
                setcookie("VipID", cp_encode($user['id'],md5('Easyes')), time() + 3600,'/');
                setcookie("VipUser", cp_encode($user['username'],md5('Easyes')), time() + 3600,'/');
                setcookie("VipName", cp_encode($user['name'],md5('Easyes')), time() + 3600,'/');
            //成功
                json(3, '注册成功',__APP__.'/mycenter');  
            }else{
                json(0, '注册失败！请重新确认注册');
            }
        }else{
            $this->display('index/freg');
        }        
    }
    //协议
    public function xieyi(){
        $site = $this->model->table('site')->field('content')->where('id=2')->find();      
        $this->assign('site', $site);
        $this->display('index/xieyi');
    }
    //验证是否
    public function formcack(){
        $id = $_GET['id'];
        $val = $_GET['value'];
        if($id == 'email'){
           $condition = "email='".$val."'";
        }elseif($id =='iphone'){
           $condition = "iphone='".$val."'";  
        }elseif($id =='username'){
           $condition = "username='".$val."'";  
        }
        $info = $this->model->table('user')->where($condition)->find(); 
        if($info){
            echo json_encode(array('success'=>false,'message'=>'有重复！请重新输入'));
        }else{
            echo json_encode(array('success'=>true));
        }
        exit;
    }
    //会员登录
    public function login(){
        $action = $_POST['action'];
        if ($action == "post") {//获取数据
            $username = in($_POST['loginuser']);
            $password = md5($_POST['password']);
            $msg = Check::rule(
                    array(check::must($_POST['loginuser']), '用户没有填写'),
                    array(check::must($_POST['password']), '密码没有填写'),
                    array(check::must($_POST['checkcode']), '验证码没有填写'),
                    array(check::same($_POST['checkcode'], $_SESSION['verify']), '验证码输入错误')
            );
            if ($msg !== true) {
                json(0, $msg);
            }
            //数据库操作
            if(check::email($_POST['loginuser'])){
                $condition['email'] = $username;
            }elseif (check::mobile($_POST['loginuser'])){
                $condition['iphone'] = $username;
            }else{
                $condition['username'] = $username;
            }
            $user = $this->model->table('user')->where($condition)->find();
            if ($user['password'] != $password) {
                json(0, '密码输入错误！');
            }
            if ($user['lock'] == 1) {
                json(0, '用户已经锁定');
            }
            setcookie("VipID", cp_encode($user['id'],md5('Easyes')), time() + 3600,'/');
            setcookie("VipUser", cp_encode($user['username'],md5('Easyes')), time() + 3600,'/');
            setcookie("VipName", cp_encode($user['name'],md5('Easyes')), time() + 3600,'/');
            //更新帐号信息
            $data=array();
            $data['ip'] = get_client_ip();
            $data['uptime'] = $_SERVER['REQUEST_TIME'];
            $up=array();
            $up['id'] = $user['id'];
            $this->model->table('user')->data($data)->where($up)->update();
            if($_POST['url']){//判断登陆成功后跳转路径
                $url = $_POST['url'];
            }else{
                $url = __APP__.'/mycenter';
            }
            json(3, '成功登录！！',$url);
        }else{
            $url = in($_GET[url]);
            $this->assign('url', $url);
            $this->display('index/login'); 
        }
    }
    public function getpassword(){
        $action = $_POST['action'];
        $close = in($_GET[close]);
        if ($action == "post") {//获取数据
            $username = in($_POST['loginuser']);
            $msg = Check::rule(
                    array(check::must($_POST['loginuser']), '用户没有填写'),
                    array(check::must($_POST['checkcode']), '验证码没有填写'),
                    array(check::same($_POST['checkcode'], $_SESSION['verify']), '验证码输入错误')
            );
            if ($msg !== true) {
                json(0, $msg);
            }
            //数据库操作
            if (check::email($_POST['loginuser'])) {
                $condition['email'] = $username;
            } elseif (check::mobile($_POST['loginuser'])) {
                $condition['iphone'] = $username;
            } else {
                $condition['username'] = $username;
            }
            $user = $this->model->table('user')->where($condition)->find();
            if ($user['lock'] == 1) {
                Error::show('帐号锁定...禁止操作', 0, __URL__ . '/getpassword?close=1');
            }
            if(!$user['email']){
                Error::show('您的信箱没有填写无法找回密码,请电话联系我们。');
            }
            $newpass = cp_uniqid();
            $emailadd     =  $user['email'];   //收件人Email地址必须填写
            $emailtitle   = '尊敬的会员：[ '. $user['username'].' ]您的帐号新密码重置信息';   //必须填写发信主题
            $emailcontent = '<style>
html{word-wrap:break-word;}
body{font-size:14px;font-family:arial,verdana,sans-serif;line-height:25px;padding:8px 10px;margin:0;}
pre {
	white-space: pre-wrap; 
	white-space: -moz-pre-wrap; 
	white-space: -pre-wrap;
	white-space: -o-pre-wrap;
	word-wrap: break-word;
}
</style>
<html>
<head>
<title>密码找回</title>
</head>
<body style="margin:0">
<div style=" margin:10px;line-height:40px; font-size:14px">
<div>亲爱的用户:'.$user['username'].'</div>
<div>您好!</div>
<div style="line-height:20px">
<div>您在'. date('Y年m月d日 h:i:s'). '提交找回密码请求，请点击下面的链接修改密码。</div>
<div>为了保障您帐号的安全性，该链接有效期为24小时，并且在您验证过一次才失效！</div>
</div>
<div><a href="' .__URL__. '/newpass.html?verifystr='.$newpass.'" target="_self">' . __URL__ . '/newpass.html?verifystr=' . $newpass . '</a></div>
<div>(如果您无法点击这个链接，请将此链接复制到浏览器地址栏后访问)</div>
<div>绑定手机号码、设置验证邮箱将更好地保障您的帐号安全，建议在' . $this->config['sitename'].'会员中心里填写完整个人信息。</div>
<div>感谢您使用' . $this->config['sitename'] . '！</div>
<div>' . $this->config['sitename'] . '会员中心</div>
<div>'. date('Y年m月d日').'</div>
</div>
</body>
</html>';     //必须填写发信内容
            Email::init($this->config);    //初始化配置 //参数1：接收人的邮箱地址；参数2：邮件标题；参数3：邮件内容
            Email::send($emailadd, $emailtitle, $emailcontent);   //发送邮件 
            $up = array();
            $up['id'] = $user['id'];
            $data = array();
            $data['restpass'] = 1;
            $data['restcode'] = $newpass;
            $data['restdate'] = time();
            $this->model->table('user')->data($data)->where($up)->update();
            Error::show('找回密码已成功发送您注册时填写的信箱！',0,__URL__.'/getpassword.html?close=1');
        } else {
            $this->assign('close', $close);
            $this->display('index/getpass');
        }
    }
    //修改密码
    public function newpass(){
        $action = $_POST['action'];
        $verifystr = in($_GET[verifystr]);
        if ($action == "post") {//获取数据
            $msg = Check::rule(
                    array(check::must($_POST['password']), '修改新密码没有填写'),
                    array(check::same($_POST['password'], $_POST['passcheck']), '两次密码输入不同,请重新输入'),
                    array(check::must($_POST['checkcode']), '验证码没有填写'),
                    array(check::same($_POST['checkcode'], $_SESSION['verify']), '验证码输入错误')
            );
            if ($msg !== true) {
                json(0, $msg);
            }
            $up = array();
            $up['restcode'] = $verifystr;
            $data = array();
            $_POST['password']=md5($_POST['password']);
            unset($_POST['passcheck']);
            unset($_POST['checkcode']);
            $data = postinput($_POST);
            $data['restpass'] = null;
            $data['restcode'] = null;
            $data['restdate'] = null;
            $result = $this->model->table('user')->data($data)->where($up)->update();
            if($result){
                json(3, '新密码已经成功修改！',__URL__.'/login.html');
            }else{
                json(3, '修改失败！请重新找回密码',__URL__.'/login.html');
            }             
        }else{
            if($verifystr){
                $condition['restcode'] = $verifystr;
                $info = $this->model->table('user')->where($condition)->find();
                if($info){
                    $locktime = $info['restdate'] + 86400;
                    $nowtime = time();
                    if ($nowtime >= $locktime){
                        Error::show('找回密码链接超时!');
                    }
                    $this->assign('verifystr', $verifystr);
                    $this->display('index/newpass');
                }
            }else{
            Error::show('亲..你是在非法操作哦!');
            } 
        }
    }
    //用户退出
    public function logout() {
        setcookie("VipID", "", time() - 3600, '/');
        setcookie("VipUser", "", time() - 3600,'/');
        setcookie("VipName", "", time() - 3600,'/');
        unset ($_COOKIE);
        $this->redirect(__APP__);
    }
    //生成验证码
    public function verify() {
        Image::buildImageVerify();
    }
}