<?php
class pageMod extends commonMod {
    public function __construct() {
        parent::__construct();
    }
    //方法:新闻分类列表
    //参数:无
    public function index() {
        $info = $this->model->table('page')->where()->select();
        $this->assign('info', $info);
        $this->display('page/index');   
    }
    //方法:阅读查看
    public function read(){
        $fid = in($_GET[0]); //读取用户
        $condition['id'] = intval($fid);
        $info = $this->model->table('page')->where($condition)->find();
        $this->assign('info', $info);
        $this->display('page/read');
    }
}