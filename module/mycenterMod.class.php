<?php
class mycenterMod extends commonMod {
    private $uid;
    private $user;
    private $uname;
    public function __construct() {
        parent::__construct();
        if (!isset($_COOKIE['VipID'])) {
            $this->redirect(__APP__ . '/index/login');
            exit();
        }
        $this->uid   = $this->uvip(1);
        $this->user  = $this->uvip(2);
        $this->uname = $this->uvip(3);
        $vip = $this->model->table('user')->field('vip')->where('id='.$this->uid)->find(); 
        $this->assign('user', $this->user);//用户名
        $this->assign('name', $this->uname);//姓名
        $this->assign('rebate',$this->userinfo($this->uid,0)); //折扣率
        $this->assign('pv', $this->userinfo($this->uid,1)); //等级名称
        $this->assign('vip', $vip);//姓名
    }
    //后台首页
    public function index() {
        $info = $this->model->table('site')->field('content')->where('id=1 or id=7')->select();
        $this->assign('site', $info[0]);
        $this->assign('info', $info[1]);
        $this->display('user/index');
    }
    //我的资料
    public function info() {
        $action = $_POST['action'];
        if ($action == 'post') {
            $msg = Check::rule(
                    array(check::must($_POST['username']), '用户名没有填写'),
                    array(check::chackone($_POST['email'], $_POST['iphone']), 'Email和手机号最少要填一项'),
                    array(check::email($_POST['email']), '信箱输入错误'),
                    array(check::mobile($_POST['iphone']), '手机号输入错误')
            );
            if ($msg !== true) {
                json(0,$msg);
            }
            $fid = $this->uid;
            $email = in($_POST['username']);
            $email = in($_POST['email']);
            $iphone = in($_POST['iphone']);
            $sql = "id <> $fid and (username = '$username' or email = '$email' or iphone ='$iphone')";
            $info = $this->model->table('user')->where($sql)->find();
            if($info){
                if ($username == $info['username']) {
                    json(0,'相同的' . $username . '已存在！');
                }
                if ($email == $info['email']) {
                    json(0,'相同的' . $email . '已存在！');
                }
                if ($iphone == $info['iphone']) {
                    json(0,'相同的' . $iphone . '已存在！');
                }
                json(0,'有相同信息禁止修改！');
            }
            $count = $this->model->table('user')->field('allvip,vip,mdycount')->where('id='.$this->uid)->find();
            if(!$count['mdycount']){
               $vip = $this->model->table('sys')->field('gold')->where('id = 2')->find();
               $_POST['mdycount'] = 1;
               $_POST['allvip'] = $count['allvip']+$vip['gold'];
               $_POST['vip'] = $count['vip']+$vip['gold'];
            }
            unset($_POST['username']);
            $condition['id'] = $fid;
            $data = postinput($_POST);
            $result = $this->model->table('user')->data($data)->where($condition)->update();
            json(3,'您的个人信息成功修改',__URL__.'/info');
        }else{
            $info = $this->model->table('user')->where('id='.$this->uid)->find();
            $this->assign('info', $info);
            $this->display('user/info');
        }
    } 
    public function formcack(){
        $id = $_GET['id'];
        $val = $_GET['value'];
        $condition = 'id<>'.$this->uid;
        if($id == 'email'){
           $condition .= " and email='".$val."'";
        }elseif($id =='iphone'){
           $condition .= " and iphone='".$val."'";  
        }
        $info = $this->model->table('user')->where($condition)->find(); 
        if($info){
            echo json_encode(array('success'=>false,'message'=>'有重复！请重新输入'));
        }else{
            echo json_encode(array('success'=>true));
        }
        exit;
    }
    //############################
    //我的订单
    public function order(){
        $act = $_GET['action'];
        if ($act == "del") {
            $fid = in($_GET[0]); 
            $condition['id']     = $fid;
            $condition['userid'] = $this->uid;
            $result = $this->model->table('order')->where($condition)->delete();
            if($result){
                echo 1;
            }else{
                echo "删除失败";
            }
        }else{
            $condition['userid'] = $this->uid;
            $info = $this->model->table('order')->where($condition)->order('id desc')->select();
            $this->assign('info', $info);
            $this->display('user/order');   
        }
    }
    //查看订单
    public function orderread(){
        $fid = in($_GET[0]); //读取用户
        $condition['id'] = intval($fid);
        $info = $this->model->table('order')->where($condition)->find();
        $this->assign('info', $info);
        $this->display('user/orderread');
    }
    //处理订单
    public function orderpass(){
        $fid = in($_GET[0]); //读取用户
        $act = $_GET['action'];
        if ($act == "goto") {
            $data['new'] = 2; 
            $condition['id'] = $fid;
            $result = $this->model->table('order')->data($data)->where($condition)->update();
            echo '完成交易';
        }  
    }
    //############################
    //联系我们
    public function about(){
        $site = $this->model->table('site')->field('content')->where('id=5')->find();      
        $this->assign('site', $site);
        $this->display('user/about');
    }
    //常见问题
    public function news(){
        $site = $this->model->table('site')->field('content')->where('id=6')->find();      
        $this->assign('site', $site);
        $this->display('user/news');
    }
    //邀请好友
    public function youme(){
        $action = $_POST['action'];
        if ($action == "post") {//删除用户 
           $youme = cp_encode($this->uid);
           $emailadd = $_POST['emailadd'];
           $emailtitle = $emailtitle   = '您的朋友[ '. $this->uname.' ],发来的邀请,请注意查收。';   //必须填写发信主题
           $emailcontent = '<style>
html{word-wrap:break-word;}
body{font-size:14px;font-family:arial,verdana,sans-serif;line-height:25px;padding:8px 10px;margin:0;}
pre {
	white-space: pre-wrap; 
	white-space: -moz-pre-wrap; 
	white-space: -pre-wrap;
	white-space: -o-pre-wrap;
	word-wrap: break-word;
}
</style>
<html>
<head>
<title>邀请邮件</title>
</head>
<body style="margin:0">
<div style=" margin:10px;line-height:40px; font-size:14px">
<div>您好，我是:'.$this->uname.'</div>
<div style="line-height:20px">
<div>我在'. date('Y年m月d日 h:i:s'). '给您发了邀请注册邮件。</div>
<div>这里有您需要的产品,非常有效.而且价格也不高,还可以直接和专家交流。</div>
</div>
<div><a href="' .__APP__. '/index/reg.html?youme='.$youme.'" target="_self">' . __APP__ . '/index/reg.html?youme=' . $youme . '</a></div>
<div>(如果您无法点击这个链接，请将此链接复制到浏览器地址栏后访问)</div>
</div>
<div style="text-align:center; font-size:12px">© baidu</div>
</body>
</html>';     //必须填写发信内容;
           $msg = Check::rule(
                   array(check::must($_POST['emailadd']), '收件人Email地址必须填写'),
                   array(check::email($_POST['emailadd']), '收件人Email地址必须填写')
                   );
           if ($msg !== true){
               json(0, $msg);
           }
           Email::init($this->config);    //初始化配置 //参数1：接收人的邮箱地址；参数2：邮件标题；参数3：邮件内容
           $result = Email::send($emailadd,$emailtitle,$emailcontent);   //发送邮件 
           if($result){
               json(3, '邀请已经成功发送给您的好友',__URL__.'/youme.html');
           }else{
               json(0, '邀请邮件发生失败,请重新发生');
           }
        }else{
            $this->assign('uname', $this->uname);
            $this->assign('youme', cp_encode($this->uid));
            $this->display('user/youme');  
        }
    }
    /* ******************
     * 会员中心留言
     * ******************/
    //会员留言列表
    function guestbook(){
        $condition['userid'] = $this->uid;
        $condition['open'] = 1;
        $info = $this->model->table('guestbook')->where($condition)->order('id desc')->select();
        $this->assign('info', $info);
        $this->display('user/guestbook');
    }
     //会员留言
    function guestpost(){
        $action = $_POST['action'];
        if ($action == "post") {//删除用户
            $msg = Check::rule(
                    array(check::must($_POST['content']), '对不起！留言内容没有填写'),
                    array(check::must($_POST['checkcode']), '验证码没有填写'),
                    array(check::same($_POST['checkcode'], $_SESSION['verify']), '验证码输入错误')
            );
            if ($msg !== true) {
                json(0,$msg);
            }
            if (!isset($_COOKIE['guestid'])) {
                json(0, '1分钟内内禁止重复留言'); 
            }
            $_POST['uptime'] = time();
            $_POST['userid'] = $this->uid;
            $_POST['username'] = $this->uname;
            unset($_POST['checkcode']);
            $data = postinput($_POST); 
            $result = $this->model->table('guestbook')->data($data)->insert();
            if($result){
                $vip = $this->model->table('sys')->field('gold')->where('id = 5')->find();
                $id = $this->uid;
                $sql = "UPDATE ".$this->model->pre."user SET allvip = allvip+".$vip['gold'].",vip = vip+".$vip['gold']." WHERE id=".$id;
                $this->model->query($sql); 
                setcookie("guestid",'1', time() + 60,'/');
                json(3, '留言成功!管理员审核后即可显示',__URL__.'/guestbook.html');
            }else{
                json(0, '对不起！数据提交失败请刷新重新提交');
            }  
        }
    }
    //生成验证码
    public function verify() {
        Image::buildImageVerify();
    }
}