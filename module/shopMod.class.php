<?php
class shopMod extends commonMod {
    public function __construct() {
        parent::__construct();
    }
    public function index() {
        $fid = in($_GET[0]); //读取用户
        if(!$fid){
            Error::show('参数错误', 0);
        }
        $condition['id']=intval($fid);
        $list = $this->model->table('product')->where($condition)->find();
        $rmb =$list['userprice'];
        if (isset($_COOKIE['VipID'])) {//判断是否会员
            $ID = intval($this->uvip(1));
            if ($list['discount'] == 1) {//折扣率
                $rebate = $this->userinfo($ID);
                $this->assign('rebate', $rebate);
                $rmb = $rmb*$rebate;//价
            }
            $vipcon['id'] = $ID;
            $vip = $this->model->table('user')->where($vipcon)->find();
            $this->assign('vip', $vip);
        }
        $this->assign('rmb', $rmb);
        $this->assign('rs', $list);
        //调用评论
        $concomment['pid']     = $fid;
        $concomment['classid'] = 1;
        $concomment['open']    = 1;
        $info = $this->model->table('comment')->where($concomment)->order('id desc')->select();
        $this->assign('info', $info);
        $this->display('shop/index');
    }
    public function buy(){
        $cat_id = intval(in($_GET[0])); //分类ID
        //分页开始
        $url = __URL__ . '/buy-' . $cat_id . '-{page}.html';
        $listRows = 20; //每页显示的信息条数 
        $page = new Page();
        $cur_page = $page->getCurPage($url);
        $limit_start = ($cur_page - 1) * $listRows;
        $limit = $limit_start . ',' . $listRows;
        //分类
        $condition['pid'] = $cat_id;
        $class = $this->model->field('id,pid,name')->table('class')->where($condition)->select();
        $this->assign('class', $class);
        unset($condition);
        //获取总行数
        if (!empty($cat_id)) {
            $condition['classid'] = $cat_id;
        }
        $allcount = $this->model->table('product')->where($condition)->count();
        $this->assign('allcount', $allcount);
        //获取折扣
        if (isset($_COOKIE['VipID'])) {//判断是否会员
            $rebate = $this->userinfo($ID);
            $this->assign('rebate', $rebate);
        }
        //读取数据开始
        $info = $this->model->table('product')->where($condition)->limit($limit)->select();
        $this->assign('page', $page->show($url, $allcount, $listRows, 10, 4));
        $this->assign('path', $this->flpath($cat_id, '/buy'));
        $this->assign('info', $info); 
        $this->display('shop/buy'); 
    }
    public function maps(){
       $cat_id = intval(in($_GET[0])); //分类ID
       $condition = array();
       $condition['pid'] = $cat_id;
       $class = $this->model->field('id,pid,name')->table('class')->where($condition)->select();
       if($class){
         foreach ($class as $value){
             $rel[$value['id']] = $this->getList($value['id']);
          } 
       }
       $this->assign('path', $this->flpath($cat_id, '/maps'));
       $this->assign('classid', $cat_id);
       $this->assign('class', $class);
       $this->assign('product', $rel);
       $this->display('shop/maps'); 
    }
    public function getList($classid = NULL){
        $condition = array();
        if (!empty($classid)) {
            $condition['classid'] = $classid;
        }
        $condition['pass'] = 1;
        $rel = $this->model->table('product')->field('id,classid,name')->where($condition)->select();
        if($rel){
            return $rel;
        }
    }
    //评论提交
    public function commentpost(){
        $action = $_POST['action'];
        if ($action == "post") {//评论提交
            $msg = Check::rule(
                    array(check::must($_POST['pid']), '评论主ID不能为空'),
                    array(check::must($_POST['content']), '对不起！评论内容没有填写')
            );
            if ($msg !== true) {
                json(0,$msg);
            }
            if ($_COOKIE['commentid'] == $_POST['pid']) {
                json(0, '同一主题10分钟内内禁止重复评论'); 
            }
            //查看是否购买过本产品
            $condition['shopid'] = intval(in($_POST['pid']));
            $condition['new']    = 2;
            $conment = $this->model->table('order')->field('shopid,new')->where($condition)->find();
            if(!$conment){
                json(0,'只有购买过本产品的会员才可以发表评论');
            }
            $_POST['uptime']  = time();
            $_POST['classid'] = 1;
            $_POST['userid']  = $this->uvip(1);
            $_POST['username']= $this->uvip(3);
            $data = postinput($_POST); 
            $result = $this->model->table('comment')->data($data)->insert();
            if($result){
                $vip = $this->model->table('sys')->field('gold')->where('id = 5')->find();
                $id = $this->uvip(1);
                $sql = "UPDATE ".$this->model->pre."user SET allvip = allvip+".$vip['gold'].",vip = vip+".$vip['gold']." WHERE id=".$id;
                $this->model->query($sql); 
                setcookie("commentid",$condition['shopid'], time() + 60,'/');
                json(3, '留言成功!管理员审核后即可显示',__URL__.'/index-'.$_POST['pid'].'.html');
            }else{
                json(0, '对不起！数据提交失败请刷新重新提交');
            }  
        }
    }
    //购买提交
    public function datepost() {
        $action = $_POST['action'];
        if ($action == 'post') {
            $msg = Check::rule(
                    array(check::must($_POST['shopid']), '订购产品ID不能为空'),
                    array(check::must($_POST['shoprmb']), '产品价格不能为空'),
                    array(check::must($_POST['shopcount']), '订购数量项不可为空'),
                    array(check::num($_POST['shopcount']), '订购数量必须为数字'),
                    array(check::must($_POST['youname']), '联系人项不可为空'),
                    array(check::must($_POST['iphone']), '联系手机项不可为空'),
                    array(check::mobile($_POST['iphone']), '请输入正确的11位手机号'),
                    array(check::email($_POST['email']), '邮箱格式不正确,请检查.正确格式例如younname@gmail.com'),
                    array(check::must($_POST['address']), '地址项不可为空'),
                    array(check::must($_POST['checkcode']), '验证码没有填写'),
                    array(check::same($_POST['checkcode'], $_SESSION['verify']), '验证码输入错误')
            );
            if ($msg !== true) {
                json(0,$msg);
            }
            //获取赠送多少积分
            $condition['id'] = in($_POST['shopid']);
            $list = $this->model->table('product')->field('give,givevip,vipprice')->where($condition)->find();
            if($list['give'] == 1){
                $givevip = $list['givevip'];
            }
            //判断是否登录用户
            if(isset ($_COOKIE['VipID'])){
                if($list['give'] == 1){//获取积分
                    $id = intval($this->uvip(1));
                    $sql = "UPDATE ".$this->model->pre."user SET allvip = allvip+".$givevip.",vip = vip+".$givevip." WHERE id=".$id;
                    $this->model->query($sql); 
                }
                $userid   = $this->uvip(1);
                $username = $this->uvip(3);
                $vipprice = in($_POST['vipprice']);
                if($vipprice && is_numeric($vipprice)){
                   $vip = $this->model->table('user')->field('vip')->where('id = '.$userid)->find();
                   if($vipprice>$vip['vip']){
                       $vipprice = $vip['vip'];
                   }
                   if($vipprice>$list['vipprice']){
                       $vipprice = $list['vipprice'];
                   }
                   $_POST['shoprmb'] = $_POST['shoprmb']-$vipprice/10;
                   $this->model->query("UPDATE ".$this->model->pre."user SET vip = vip-".$vipprice." WHERE id=".$id); 
                }
            }else{//自动注册帐号
                $reg['username'] = in($_POST['iphone']);
                $reg['name']     = in($_POST['youname']);
                $reg['email']    = in($_POST['email']);
                $reg['iphone']   = in($_POST['iphone']);
                $reg['address']  = in($_POST['address']);
                $reg['password'] = md5($_POST['iphone']);
                $reg['regtime']  = time();
                $reg['regtype']  = 1;
                //获取积分
                $convip['id'] = 1;
                $vip = $this->model->table('sys')->where($convip)->find();
                $reg['allvip']  = $givevip+$vip['gold'];
                $reg['vip']     = $givevip+$vip['gold'];
                //入库
                $regdata = postinput($reg);
                $this->model->table('user')->data($regdata)->insert();
                $userid   = mysql_insert_id();
                $username = $reg['name'];
            }
            //订购订单入库
            unset($_POST['checkcode']);
            unset($_POST['vipprice']);
            $_POST['shoptime']   = time();
            $_POST['usernumber'] = rand(100000,999999);
            $_POST['userid']     = $userid;
            $_POST['username']   = $username;
            $_POST['name']       = $_POST['youname'];
            unset($_POST['youname']);
            $data = postinput($_POST);
            $this->model->table('order')->data($data)->insert();
            json(3, '产品购买成功,可登录会员中心查看购买的产品',__URL__."/shopmsg.html");
        }else{
            Error::show('参数错误', 0);
        }
    }
    //购买成功页面
    public function shopmsg(){
        $info = $this->model->table('site')->cache(10)->field('content')->where('id=3')->find();
        $this->assign('info', $info);
        $this->display('shop/shopmsg');
    }
    //折扣说明
    public function zk(){
        $info = $this->model->table('site')->cache(10)->field('title,content')->where('id=8')->find();
        $this->assign('info', $info);
        $this->display('shop/read');
    }
    //换购说明
    public function hg(){
        $info = $this->model->table('site')->cache(10)->field('title,content')->where('id=9')->find();
        $this->assign('info', $info);
        $this->display('shop/read');
    }
    //赠送积分
    public function zs(){
        $info = $this->model->table('site')->cache(10)->field('title,content')->where('id=10')->find();
        $this->assign('info', $info);
        $this->display('shop/read');
    }
    //方法:当前分类的位置
    //参数：pid栏目ID,action当前位置栏目连接页面
    protected function flpath($pid = 0, $action) {
        $condition['id'] = intval($pid);
        $pname = $this->model->table('class')->field('id,pid,name')->where($condition)->find();
        if ($pname) {
            $Str = $this->flpath($pname['pid'], $action);
            $Str .= '<a href="' . __URL__ . $action . '-' . $pname['id'] . '.html' . '">' . $pname['name'] . '</a> >> ';
        }
        return $Str;
    }
    //生成验证码
    public function verify() {
        Image::buildImageVerify();
    }
}