<?php
//错误输出类
class Error {
    /*     * *************************************************
     * 方 法 名: show
     * 功能描述:错误输出提示
     *  */
    static public function show($message, $back = null, $url = null) {
        echo '<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8"> 
<title>系统提示!</title>
<script style="text/javascript">
    function redirect(url){
        location.href = url;
    }
</script>
<style type="text/css">
<!--
a:link,a:visited{text-decoration:none;color:#0068a6}
a:hover,a:active{color:#ff6600;text-decoration:none;}
-->
</style>
</head>
<body>
<div style="border:1px solid #E3E7E9;width:400px;position:absolute;top:45%;left:50%;margin:-80px 0 0 -200px">
<div style="border:1px solid #fff; padding:15px; background:#F6F6F6;">
<div style="border-bottom:1px #CCC solid; font-size:26px;font-family: "Microsoft Yahei", Verdana, arial, sans-serif; line-height:40px; height:40px; font-weight:bold">操作提示!</div>
<div style="height:20px; border-top:1px solid #fff"></div>
<div style="border:1px dotted #F90; border-left:6px solid #F60; padding:15px; background:#FFC">' . $message . '</div>
<div style="height:20px;"></div>
<div style=" font-size:15px;">您可以选择&nbsp;&nbsp;<a href="' . $_SERVER['PHP_SELF'] . '" title="重试">重试</a>&nbsp;&nbsp;<a href="javascript:history.back()" title="返回">返回</a>&nbsp;&nbsp;或者&nbsp;&nbsp;<a href="/" title="回到首页" target="_top">回到首页</a> </div>
</div>
</div>
</body>
</html>';
if ($back)
    $url = "javascript:history.back();";
if ($url)
    echo '<script language="javascript">setTimeout("redirect(\''.$url.'\');", 2000);</script>';
exit();
    }
}