-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2012 年 03 月 13 日 06:20
-- 服务器版本: 5.5.16
-- PHP 版本: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `vipuser`
--

-- --------------------------------------------------------

--
-- 表的结构 `vip_admin`
--

DROP TABLE IF EXISTS `vip_admin`;
CREATE TABLE IF NOT EXISTS `vip_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_user` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_pass` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_lock` int(2) DEFAULT NULL,
  `admin_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_ip` char(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_date` int(11) DEFAULT NULL,
  `admin_text` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `vip_admin`
--

INSERT INTO `vip_admin` (`admin_id`, `admin_user`, `admin_pass`, `admin_lock`, `admin_name`, `admin_ip`, `admin_date`, `admin_text`, `group_id`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 2, '徐子峰', '127.0.0.1', 1331616858, '超级管理员帐号..一般禁止删除操作', 1);

-- --------------------------------------------------------

--
-- 表的结构 `vip_ask`
--

DROP TABLE IF EXISTS `vip_ask`;
CREATE TABLE IF NOT EXISTS `vip_ask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classid` int(11) DEFAULT NULL,
  `radio` int(1) DEFAULT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- 转存表中的数据 `vip_ask`
--

INSERT INTO `vip_ask` (`id`, `classid`, `radio`, `name`) VALUES
(1, 2, 1, '我使用购物袋替代塑料袋是因为不愿承担购买塑料袋的费用'),
(9, 2, 2, 'asdfasdf');

-- --------------------------------------------------------

--
-- 表的结构 `vip_askclass`
--

DROP TABLE IF EXISTS `vip_askclass`;
CREATE TABLE IF NOT EXISTS `vip_askclass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vip` int(20) DEFAULT NULL,
  `starttime` int(20) DEFAULT NULL,
  `endtime` int(20) DEFAULT NULL,
  `titlekey` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `vip_askclass`
--

INSERT INTO `vip_askclass` (`id`, `title`, `about`, `vip`, `starttime`, `endtime`, `titlekey`, `keywords`, `description`) VALUES
(2, '关于“限塑令”的调查问卷', ' 关于限塑令的调查本次调查会员可获得20积分！积分可用作换购商品', 20, 1330531200, 1333036800, '关于“限塑令”的调查问卷', '关于“限塑令”的调查问卷', '关于“限塑令”的调查问卷');

-- --------------------------------------------------------

--
-- 表的结构 `vip_class`
--

DROP TABLE IF EXISTS `vip_class`;
CREATE TABLE IF NOT EXISTS `vip_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT '0',
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `update` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- 转存表中的数据 `vip_class`
--

INSERT INTO `vip_class` (`id`, `pid`, `name`, `update`, `about`) VALUES
(6, 0, '祛红血丝系列', '1328837121', '祛红血丝系列'),
(7, 0, '皮肤过敏套装', '1328841763', '皮肤过敏套装');

-- --------------------------------------------------------

--
-- 表的结构 `vip_comment`
--

DROP TABLE IF EXISTS `vip_comment`;
CREATE TABLE IF NOT EXISTS `vip_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uptime` int(20) DEFAULT NULL,
  `reply` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `replytime` int(20) DEFAULT NULL,
  `new` int(2) DEFAULT '2' COMMENT '最新',
  `open` int(2) DEFAULT '0' COMMENT '核审',
  `pid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- 转存表中的数据 `vip_comment`
--

INSERT INTO `vip_comment` (`id`, `userid`, `username`, `content`, `uptime`, `reply`, `replytime`, `new`, `open`, `pid`, `classid`) VALUES
(13, 3, '夏雨凤', '适应于脸上皮肤敏感，红血丝症状较轻者，有过敏史，毛细血管循环出现障碍，毛细血管弹性、韧性降低，略有毛细血管扩张现象', 1331447816, '看看是什么原因', 1331449738, 1, 1, 3, 1),
(14, 3, '夏雨凤', '食物、金属、化妆品等过敏原引起的红血丝及皮肤瘙痒问题，伴随皮肤发红、起疹子等现象', 1331447867, NULL, NULL, 2, 1, 3, 1),
(15, 3, '夏雨凤', 'ddddddd', 1331544721, NULL, NULL, 2, 0, 3, 1);

-- --------------------------------------------------------

--
-- 表的结构 `vip_grade`
--

DROP TABLE IF EXISTS `vip_grade`;
CREATE TABLE IF NOT EXISTS `vip_grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vip` int(11) DEFAULT NULL,
  `rebate` double(10,2) DEFAULT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `vip_grade`
--

INSERT INTO `vip_grade` (`id`, `vip`, `rebate`, `name`, `about`) VALUES
(1, 5000, 0.85, '初级会员', '新注册会员！'),
(2, 10000, 0.00, '铁牌会员', '铁牌会员'),
(3, 20000, 0.00, '铜牌会员', '铜牌会员'),
(4, 50000, 0.00, '银牌会员', '银牌会员'),
(5, 1000000, 2.00, '金牌会员', '金牌会员');

-- --------------------------------------------------------

--
-- 表的结构 `vip_group`
--

DROP TABLE IF EXISTS `vip_group`;
CREATE TABLE IF NOT EXISTS `vip_group` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `power_value` varchar(1000) NOT NULL,
  `update` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

--
-- 转存表中的数据 `vip_group`
--

INSERT INTO `vip_group` (`id`, `name`, `power_value`, `update`) VALUES
(1, '超级管理员', '-1', '1318860709'),
(29, '222', '8,9,14', '1331619006');

-- --------------------------------------------------------

--
-- 表的结构 `vip_guestbook`
--

DROP TABLE IF EXISTS `vip_guestbook`;
CREATE TABLE IF NOT EXISTS `vip_guestbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uptime` int(20) DEFAULT NULL,
  `reply` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `replytime` int(20) DEFAULT NULL,
  `new` int(2) DEFAULT '2',
  `open` int(2) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- 转存表中的数据 `vip_guestbook`
--

INSERT INTO `vip_guestbook` (`id`, `userid`, `username`, `content`, `uptime`, `reply`, `replytime`, `new`, `open`) VALUES
(7, 3, '夏雨凤', 'ddddddddddddddddddddddddd', 1329985031, NULL, NULL, 2, 1),
(8, 3, '夏雨凤', 'asdf', 1330064963, NULL, NULL, 2, 1),
(9, 3, '夏雨凤', '测试测试', 1331117712, '测测', 1331372578, 1, 0),
(10, 3, '夏雨凤', '00', 1331539296, NULL, NULL, 2, 0);

-- --------------------------------------------------------

--
-- 表的结构 `vip_news`
--

DROP TABLE IF EXISTS `vip_news`;
CREATE TABLE IF NOT EXISTS `vip_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `pid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `titlekey` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `count` int(11) DEFAULT '1',
  `best` int(1) DEFAULT '2',
  `new` int(1) DEFAULT '2',
  `top` int(1) DEFAULT '2',
  `pass` int(1) DEFAULT '2',
  `comment` int(1) DEFAULT '2',
  `uptime` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `vip_news`
--

INSERT INTO `vip_news` (`id`, `userid`, `username`, `classid`, `pid`, `title`, `images`, `content`, `titlekey`, `keywords`, `description`, `count`, `best`, `new`, `top`, `pass`, `comment`, `uptime`) VALUES
(3, NULL, NULL, 6, NULL, '敏感型红血丝套装(轻度)1', NULL, '&nbsp;适应症状：<br />\r\n1、适应于脸上皮肤敏感，红血丝症状较轻者，有过敏史，毛细血管循环出现障碍，毛细血管弹性、韧性降低，略有毛细血管扩张现象；<br />\r\n2、美容换肤、局部长期使用皮质类激素药物、缺乏维生素或微量元素、冻伤等形成的红血丝，斑点状、星网状、蜘蛛爪状红血丝开始出现；<br />\r\n3、食物、金属、化妆品等过敏原引起的红血丝及皮肤瘙痒问题，伴随皮肤发红、起疹子等现象。\r\n<p>\r\n	&nbsp;\r\n</p>\r\n<span style="color:#000000;">红血丝是由于脸部毛细血管扩张引起的，患者脸部看上去比一般的肤色要红，有的只是脸颊两侧发红，边界圆形。这类肤质很敏感，过冷、过热、情绪激动都会使得面色更红，严重的还会出现沉积性色斑，不仅影响外表形象，而且会给心理带来压力，影响个人工作和生活。很多人面部肌肤的角质层很薄，毛细血管分布得浅，因此，对于外界的刺激非常敏感，红血丝就容易出现。气候的变化对敏感性肌肤的影响很大，春天的花粉、夏天的紫外线、秋天的干燥以及冬天的低温都会引发肌肤过敏从而出现红血丝，各类化妆品、金属、食物、药物等过敏源都非常有可能引起红血丝，严重者会出现灼伤感、皮肤瘙痒、起疹子、色素成着、猪肝红等症状。1</span>', NULL, NULL, NULL, 1, 1, 1, 1, 1, 1, NULL),
(4, NULL, NULL, 6, NULL, 'aaaaaaaaaaaaaaa', NULL, 'aaaaaaaaaaaaaaaaaaaa', 'as', 'as', 'asdf', 1, 2, 2, 2, 2, 2, 1330245929);

-- --------------------------------------------------------

--
-- 表的结构 `vip_newsclass`
--

DROP TABLE IF EXISTS `vip_newsclass`;
CREATE TABLE IF NOT EXISTS `vip_newsclass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT '0',
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `update` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- 转存表中的数据 `vip_newsclass`
--

INSERT INTO `vip_newsclass` (`id`, `pid`, `name`, `update`, `about`) VALUES
(6, 0, '祛红血丝系列', '1330411734', '祛红血丝系列'),
(7, 0, '皮肤过敏套装1', '1330065658', '皮肤过敏套装');

-- --------------------------------------------------------

--
-- 表的结构 `vip_order`
--

DROP TABLE IF EXISTS `vip_order`;
CREATE TABLE IF NOT EXISTS `vip_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usernumber` int(10) DEFAULT NULL,
  `shopid` int(11) DEFAULT NULL,
  `shopname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shoprmb` double(10,2) DEFAULT NULL,
  `shopcount` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iphone` int(20) DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shoptime` int(20) DEFAULT NULL,
  `new` int(2) DEFAULT NULL COMMENT '0新单1发货2收货',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=27 ;

--
-- 转存表中的数据 `vip_order`
--

INSERT INTO `vip_order` (`id`, `userid`, `username`, `usernumber`, `shopid`, `shopname`, `shoprmb`, `shopcount`, `name`, `iphone`, `email`, `address`, `about`, `shoptime`, `new`) VALUES
(20, 3, '夏雨凤', 466476, 3, '敏感型红血丝套装(轻度)1', -20.00, 1, '夏雨凤', 2147483647, 'ltmn@qq.com', '北京市海淀区苏州街20号院银丰大厦2号楼', NULL, 1330679722, 2),
(21, 3, '夏雨凤', 980828, 3, '敏感型红血丝套装(轻度)1', 695.30, 1, '夏雨凤', 2147483647, 'ltmn@qq.com', '北京市海淀区苏州街20号院银丰大厦2号楼', NULL, 1331367692, NULL),
(22, 3, '夏雨凤', 654699, 3, '敏感型红血丝套装(轻度)1', 695.30, 1, '夏雨凤', 2147483647, 'ltmn@qq.com', '北京市海淀区苏州街20号院银丰大厦2号楼', NULL, 1331446702, NULL),
(23, 3, '夏雨凤', 910296, 3, '敏感型红血丝套装(轻度)1', NULL, 1, '夏雨凤', 2147483647, 'ltmn@qq.com', '北京市海淀区苏州街20号院银丰大厦2号楼', NULL, 1331446710, NULL),
(24, 3, '夏雨凤', 123455, 3, '敏感型红血丝套装(轻度)1', NULL, 1, '夏雨凤', 2147483647, 'ltmn@qq.com', '北京市海淀区苏州街20号院银丰大厦2号楼', NULL, 1331451143, NULL),
(25, 3, '夏雨凤', 680105, 3, '敏感型红血丝套装(轻度)1', NULL, 1, '夏雨凤', 2147483647, 'ltmn@qq.com', '北京市海淀区苏州街20号院银丰大厦2号楼', NULL, 1331451155, NULL),
(26, 3, '夏雨凤', 950012, 3, '敏感型红血丝套装(轻度)1', NULL, 1, '夏雨凤', 2147483647, 'ltmn@qq.com', '北京市海淀区苏州街20号院银丰大厦2号楼', NULL, 1331451183, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `vip_page`
--

DROP TABLE IF EXISTS `vip_page`;
CREATE TABLE IF NOT EXISTS `vip_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `uptime` int(20) DEFAULT NULL,
  `titlekey` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `vip_page`
--

INSERT INTO `vip_page` (`id`, `title`, `content`, `uptime`, `titlekey`, `keywords`, `description`) VALUES
(3, '敏感型红血丝套装(轻度)1', '&nbsp;适应症状：<br />\r\n1、适应于脸上皮肤敏感，红血丝症状较轻者，有过敏史，毛细血管循环出现障碍，毛细血管弹性、韧性降低，略有毛细血管扩张现象；<br />\r\n2、美容换肤、局部长期使用皮质类激素药物、缺乏维生素或微量元素、冻伤等形成的红血丝，斑点状、星网状、蜘蛛爪状红血丝开始出现；<br />\r\n3、食物、金属、化妆品等过敏原引起的红血丝及皮肤瘙痒问题，伴随皮肤发红、起疹子等现象。\r\n<p>\r\n	&nbsp;\r\n</p>\r\n<span style="color:#000000;">红血丝是由于脸部毛细血管扩张引起的，患者脸部看上去比一般的肤色要红，有的只是脸颊两侧发红，边界圆形。这类肤质很敏感，过冷、过热、情绪激动都会使得面色更红，严重的还会出现沉积性色斑，不仅影响外表形象，而且会给心理带来压力，影响个人工作和生活。很多人面部肌肤的角质层很薄，毛细血管分布得浅，因此，对于外界的刺激非常敏感，红血丝就容易出现。气候的变化对敏感性肌肤的影响很大，春天的花粉、夏天的紫外线、秋天的干燥以及冬天的低温都会引发肌肤过敏从而出现红血丝，各类化妆品、金属、食物、药物等过敏源都非常有可能引起红血丝，严重者会出现灼伤感、皮肤瘙痒、起疹子、色素成着、猪肝红等症状。1</span>', 1330246186, '11', '1', '1');

-- --------------------------------------------------------

--
-- 表的结构 `vip_product`
--

DROP TABLE IF EXISTS `vip_product`;
CREATE TABLE IF NOT EXISTS `vip_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classid` int(11) DEFAULT NULL,
  `specialid` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `titlekey` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marketprice` varchar(20) COLLATE utf8_unicode_ci DEFAULT '0',
  `userprice` varchar(20) COLLATE utf8_unicode_ci DEFAULT '0',
  `downdate` int(20) DEFAULT NULL,
  `vip` int(11) DEFAULT '2' COMMENT '积分',
  `vipprice` varchar(11) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '分积',
  `discount` int(2) DEFAULT '2' COMMENT '折扣',
  `best` int(2) DEFAULT '2' COMMENT '推荐',
  `give` int(2) DEFAULT '2' COMMENT '赠送积分',
  `givevip` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '赠送积分',
  `pass` int(1) DEFAULT '2',
  `comment` int(1) DEFAULT '2',
  `uptime` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `vip_product`
--

INSERT INTO `vip_product` (`id`, `classid`, `specialid`, `name`, `images`, `content`, `titlekey`, `keywords`, `description`, `marketprice`, `userprice`, `downdate`, `vip`, `vipprice`, `discount`, `best`, `give`, `givevip`, `pass`, `comment`, `uptime`) VALUES
(3, 6, 7, '敏感型红血丝套装(轻度)1', 'a24befdb042ef90f58444e57f1b9e90a.gif', '&nbsp;适应症状：<br />\r\n1、适应于脸上皮肤敏感，红血丝症状较轻者，有过敏史，毛细血管循环出现障碍，毛细血管弹性、韧性降低，略有毛细血管扩张现象；<br />\r\n2、美容换肤、局部长期使用皮质类激素药物、缺乏维生素或微量元素、冻伤等形成的红血丝，斑点状、星网状、蜘蛛爪状红血丝开始出现；<br />\r\n3、食物、金属、化妆品等过敏原引起的红血丝及皮肤瘙痒问题，伴随皮肤发红、起疹子等现象。\r\n<p>\r\n	&nbsp;\r\n</p>\r\n<span style="color:#000000;">红血丝是由于脸部毛细血管扩张引起的，患者脸部看上去比一般的肤色要红，有的只是脸颊两侧发红，边界圆形。这类肤质很敏感，过冷、过热、情绪激动都会使得面色更红，严重的还会出现沉积性色斑，不仅影响外表形象，而且会给心理带来压力，影响个人工作和生活。很多人面部肌肤的角质层很薄，毛细血管分布得浅，因此，对于外界的刺激非常敏感，红血丝就容易出现。气候的变化对敏感性肌肤的影响很大，春天的花粉、夏天的紫外线、秋天的干燥以及冬天的低温都会引发肌肤过敏从而出现红血丝，各类化妆品、金属、食物、药物等过敏源都非常有可能引起红血丝，严重者会出现灼伤感、皮肤瘙痒、起疹子、色素成着、猪肝红等症状。</span>', '细血管循环出现障碍，毛细血管弹性、韧性降低', '细血管循环出现障碍，毛细血管弹性、韧性降低，略有毛细血管扩张现象', '红血丝是由于脸部毛细血管扩张引起的，患者脸部看上去比一般的肤色要红，有的只是脸颊两侧发红，边界圆形。这类肤质很敏感，过冷、过热、情绪激动都会使得面色更红，严重的还会出现沉积性色斑，不仅影响外表形象，而且会给心理带来压力', '9651', '818', 1330185600, 1, '20000', 1, 2, 1, '500', 2, 2, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `vip_resource`
--

DROP TABLE IF EXISTS `vip_resource`;
CREATE TABLE IF NOT EXISTS `vip_resource` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `operate` varchar(255) CHARACTER SET utf8 NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `list` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=27 ;

--
-- 转存表中的数据 `vip_resource`
--

INSERT INTO `vip_resource` (`id`, `pid`, `operate`, `name`, `list`) VALUES
(1, 0, 'manage', '管理设置', 0),
(2, 1, 'index|edit', '管理员管理', NULL),
(3, 1, 'group|groupedit|getAction', '权限组管理', NULL),
(4, 1, 'vip|vipedit', '积分管理', NULL),
(5, 1, 'grade|gradeedit', '会员等级', NULL),
(6, 1, 'site|siteedit', '参数设置', NULL),
(7, 0, 'user', '会员管理', NULL),
(8, 7, 'index|read', '会员查看', NULL),
(9, 7, 'edit', '会员编辑', NULL),
(10, 7, 'sms|email', '发短信或信箱', NULL),
(11, 7, 'order|orderpass|orderread', '订单管理', NULL),
(12, 0, 'buy', '商品管理', NULL),
(13, 12, 'buyclass|classdel|classedit', '商品分类', NULL),
(14, 12, 'special|specialedit', '商品特性', NULL),
(15, 12, 'index|read|edit|comment|pass', '商品管理', NULL),
(16, 0, 'new', '新闻公告', NULL),
(17, 16, 'newsclass|classdel|classedit', '新闻分类', NULL),
(18, 16, 'index|read|edit|comment|pass|news|top|best', '新闻管理', NULL),
(19, 16, 'page|pageedit|pageread', '单页管理', NULL),
(20, 0, 'guestbook', '留言评论', NULL),
(21, 20, 'index|open|edit', '留言管理', NULL),
(22, 20, 'comment|commentopen|commentedit', '评论管理', NULL),
(23, 0, 'ask', '问卷调查', NULL),
(24, 23, 'index|edit|asklist|delvate|askedit', '问卷调查', NULL),
(25, 23, 'vate|vatecount', '调查结果', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `vip_site`
--

DROP TABLE IF EXISTS `vip_site`;
CREATE TABLE IF NOT EXISTS `vip_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `about` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- 转存表中的数据 `vip_site`
--

INSERT INTO `vip_site` (`id`, `title`, `content`, `about`) VALUES
(1, '会员中心', '&nbsp;美国超强 Y&amp;H Creature Technology CO.LTD 成立于1996年，位于美国加利福尼亚州沃纳特市。其生物工程研究中心是美国最具权威的生物基因科技机构之一，由来自美国和加拿大百余名医学领域卓有建树的专家学者组成。公司致力于纯天然健康食品的开发利用，把生物科技最新科研成果，运用最先进的生产工艺，研制出美国超强系列高科技纯天然保健食品，获得美国FDA认证。所有产品均由国际养生保健协会监制，并获得国际抗衰老协会重点推荐。在美国白领和政府官员的保健品常用名单中稳居高位。功效明显，信用卓著。其亚洲服务中心成立于2004年5月。目前已开展全国3000多个县市实现货到付款业务。预计未来两年内，在中国开2000家专卖店或代理商。现寻求双赢合作伙伴，共享巨大的市场蛋糕。随时网上订货，24小时热线0755-89812687，E-mail：aaayk@126.com，当天发货，4天通达全国。', '会员中心首页的说明'),
(2, '注册协议', '注册协议', '注册会员时，提示的注册协议'),
(3, '购买订单', '尊敬的会员您好:<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;1、您购买的产品已成功提交,<span style="color:#e53333;">如您不是会员</span><span style="color:#e53333;">我们为您自动注册了会员帐号，可使用购买时填写的姓名或手机或email登陆，密码默认是是您填写的手机号。</span>请您及时登录查看您的订单信息。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;2、新会员我们将会为您赠送一定数量的积分，积分可以抵价购买、换购和免费领取产品。 <a title="会员登录" href="http://vip.my.cn/index.php/index/login">点击这里登录:会员中心</a>', '购买订单提交成功后的说明'),
(5, '联系客服', '联系客服联系客服联系客服联系客服联系客服联系客服联系客服联系客服联系客服联系客服联系客服', '联系客服'),
(6, '常见问题', '会员中心常见问题', '会员中心常见问题'),
(7, '会员公告', '会员公告', '会员公告'),
(8, '会员折扣', '会员折扣规则说明', '会员折扣规则说明'),
(9, '积分换购', '积分换购规则说明', '积分换购规则说明'),
(10, '积分赠送', '赠送积分规则说明', '赠送积分规则说明');

-- --------------------------------------------------------

--
-- 表的结构 `vip_special`
--

DROP TABLE IF EXISTS `vip_special`;
CREATE TABLE IF NOT EXISTS `vip_special` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT '0',
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `update` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- 转存表中的数据 `vip_special`
--

INSERT INTO `vip_special` (`id`, `pid`, `name`, `update`, `about`) VALUES
(7, 0, '祛红血丝', '1328837457', '祛红血丝(轻、中、重、顽)');

-- --------------------------------------------------------

--
-- 表的结构 `vip_sys`
--

DROP TABLE IF EXISTS `vip_sys`;
CREATE TABLE IF NOT EXISTS `vip_sys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gold` int(11) DEFAULT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `vip_sys`
--

INSERT INTO `vip_sys` (`id`, `gold`, `name`, `about`) VALUES
(1, 1000, '注册会员', '第一次成功注册会员获取的积分'),
(2, 50, '完善资料', '填写完整个人资料获取积分'),
(3, 100, '公开信息', '公开文章评论和留言的积分'),
(4, 100, '邀请注册', '会员邀请好友注册'),
(5, 1, '留言评论', '建议数字不要太大！1--5之间最好');

-- --------------------------------------------------------

--
-- 表的结构 `vip_user`
--

DROP TABLE IF EXISTS `vip_user`;
CREATE TABLE IF NOT EXISTS `vip_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sex` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qq` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iphone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `allvip` int(11) DEFAULT '0',
  `vip` int(10) DEFAULT '0',
  `open` int(2) DEFAULT '1' COMMENT '否是公开',
  `lock` int(11) DEFAULT '2',
  `ip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uptime` int(20) DEFAULT NULL,
  `regtime` int(20) DEFAULT NULL,
  `regtype` int(1) DEFAULT NULL,
  `restpass` int(2) DEFAULT NULL,
  `restcode` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `restdate` int(20) DEFAULT NULL,
  `mdycount` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- 转存表中的数据 `vip_user`
--

INSERT INTO `vip_user` (`id`, `username`, `password`, `name`, `sex`, `email`, `qq`, `iphone`, `address`, `birthday`, `about`, `allvip`, `vip`, `open`, `lock`, `ip`, `uptime`, `regtime`, `regtype`, `restpass`, `restcode`, `restdate`, `mdycount`) VALUES
(3, 'ltmn', 'e10adc3949ba59abbe56e057f20f883e', '夏雨凤', '男', 'ltmn@qq.com', '81760805', '13554999729', '北京市海淀区苏州街20号院银丰大厦2号楼', '2012-02-08', '我的个人介绍！', 7237, 7237, 1, 2, '127.0.0.1', 1331543513, 1329486472, NULL, 1, 'f8ba415dc3cd13d8e1d9', 1329042554, 1),
(4, 'baidu', 'e10adc3949ba59abbe56e057f20f883e', '333', '男', 'vip@my.cn', NULL, '13011112222', NULL, NULL, NULL, 0, 0, 1, 2, '127.0.0.1', 1328853857, 1330413564, NULL, NULL, NULL, NULL, NULL),
(5, 'sina', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 'ltmn@163.com', NULL, '13011226633', NULL, NULL, NULL, 1000, 1000, 1, 2, NULL, NULL, 1328884299, NULL, NULL, NULL, NULL, NULL),
(6, 'sohu', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 'baidu@163.com', NULL, '13011226600', NULL, NULL, NULL, 1000, 1000, 1, 2, NULL, NULL, 1328884416, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `vip_vate`
--

DROP TABLE IF EXISTS `vip_vate`;
CREATE TABLE IF NOT EXISTS `vip_vate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classid` int(11) DEFAULT NULL,
  `askid` int(11) DEFAULT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vatecount` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=35 ;

--
-- 转存表中的数据 `vip_vate`
--

INSERT INTO `vip_vate` (`id`, `classid`, `askid`, `name`, `vatecount`) VALUES
(7, 2, 1, 'aa1', 5),
(9, 2, 1, 'aa3', 4),
(10, 2, 1, 'ab4', 1),
(31, 2, 9, 'a', 4),
(32, 2, 9, 'a', 1),
(33, 2, 9, 'a', 1),
(34, 2, 9, 'a', 4);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
