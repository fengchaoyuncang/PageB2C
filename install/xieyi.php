<?php 
//如果已经安装，跳转到首页
if(file_exists('../data/install.lock')){
    header("location:../");
}
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>易讯网络PHP系列软件产品-安装向导</title>

<link href="css/css.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="main">
  <div class="top">
    <div class="logo"></div>
    <div id="tool">联系我们&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.easyes.com.cn">  官方网站</a></div>
  </div>
  <div class="central">
    <div id="left">
     <ul>
       <li>
           <h1>1</h1>
           <div class="left_title">  
               <h2>准备安装</h2>
               <p>欢迎您使用易讯网络PHP系列软件产品！</p>
           </div>
       </li>
   <li>
           <h1 class="install">2</h1>
         <div class="left_title">  
         <h2 class="install">阅读协议</h2>
         <p class="install">请认真阅读软件使用协议，以免您的利益受到损害！</p>
         </div>
       </li>
       <li>
           <h1>3</h1>
         <div class="left_title">  
         <h2>基本设置</h2>
         <p>请设置软件的基本信息进行网站安装！</p>
         </div>
       </li>
       <li>
          <h1>4</h1>
         <div class="left_title">  
         <h2>开始安装</h2>
         <p>开始愉快的软件安装之旅吧！</p>
         </div>
       </li>
     </ul>
    </div>
    <div class="right">
      <div class="right_title">软件使用协议</div>
      <div style="text-align:left; line-height:25px; margin-top:20px; font-size:14px; text-indent: 2em">
        <p>感谢您选择易讯电子商务会员管理系统，本系统基于PHP+MYSQL ，采用MVC框架进行开发。<br>
                为了使你正确并合法的使用本软件，请你在使用前务必阅读清楚下面的协议条款：</p>
        <p>一、本“软件”是由林州众乐科技有限公司开发。“软件”的一切版权、商标权、专利权、商业秘密等知识产权，以及与“软件”相关的所有信息内容，包括但不限于：文字表述及其组合、图标、图饰、图表、色彩、界面设计、版面框架、有关数据、印刷材料、或电子文档等均受中华人民共和国著作权法、商标法、专利法、反不正当竞争法和相应的国际条约以及其他知识产权法律法规的保护，除涉及第三方授权的软件或技术外，林州众乐科技有限公司享有上述知识产权。  </p>
        <p>二、未经林州众乐科技有限公司书面同意，用户不得为任何营利性或非营利性的目的自行实施、利用、转让或许可任何三方实施、利用、转让上述知识产权，林州众乐科技有限公司保留追究上述未经许可行为的权利。 
            <p>三、保留权利：未明示授权的其他一切权利仍归林州众乐科技有限公司所有，用户使用其他权利时须另外取得林州众乐科技有限公司的书面同意。 </p>
            <p>四、您必须保留相关版权信息。</p>
        <p>五、免责声明<br />
         <div class="agree"  align="center">
      <form action="database.php"><input hidefocus="true" type="submit" class="button" value="马上进入下一步！" /> </form> </div>
     </div>
    </div>
  </div>
</div>
<div class="foot"></div>
</body>
</html>