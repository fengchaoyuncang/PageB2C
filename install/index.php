<?php 
//如果已经安装，跳转到首页
if(file_exists('../data/install.lock')){
    header("location:../");
}
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8" />
<title>易讯网络PHP系列软件产品-安装向导</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="main">
  <div class="top">
    <div class="logo"></div>
    <div id="tool">联系我们&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.easyes.com.cn">  官方网站</a></div>
  </div>
  <div class="central">
    <div id="left">
     <ul>
       <li>
           <h1 class="install">1</h1>
         <div class="left_title">  
         <h2 class="install">准备安装</h2>
         <p class="install">欢迎您使用易讯网络PHP系列软件产品！</p>
         </div>
       </li>
   <li>
           <h1>2</h1>
         <div class="left_title">  
         <h2>阅读协议</h2>
         <p>请认真阅读软件使用协议，以免您的利益受到损害！</p>
         </div>
       </li>
       <li>
           <h1>3</h1>
         <div class="left_title">  
         <h2>基本设置</h2>
         <p>请设置软件的基本信息进行网站安装！</p>
         </div>
       </li>
       <li>
          <h1>4</h1>
         <div class="left_title">  
         <h2>开始安装</h2>
         <p>开始愉快的软件安装之旅吧！</p>
         </div>
       </li>
     </ul>
    </div>
    <div class="right">
      <div class="right_title">软件使用说明</div>
    
    <div style="text-align:left; line-height:30px; margin-top:20px; font-size:14px; text-indent: 2em">
        <p> 易讯电子商务会员管理系统是一款简洁、高效的B/S行业管理软件。</p>
        <p> 软件采用了强大PHP+MYSQL作为程序运行基础，具有高效率、高负载量、灵活、简单的特点。</p>
        <p> 易讯电子商务会员管理系统将程序进行分离采用了模块化、插件化的管理方法，使用程序跟组装电脑一样，想要什么功能模块就给添加进来，不想要的功能模块就把删除，使它成为高度DIY的一款组装式程序，可大可小，可强可弱完全由您而定！</p>
         <p> 使用前请认真阅读本软件使用协议。</p>
        <div class="clear"></div>
         <div class="agree"  align="center">
      <form action="xieyi.php">
         	<input hidefocus="true" type="submit" class="button" value="马上进入下一步！" />
                </form> </div>
      	
     </div>
     
    </div>
  </div>
</div>
<div class="foot"></div>
</body>
</html>