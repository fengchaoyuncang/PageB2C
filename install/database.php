<?php
//如果已经安装，跳转到首页
if(file_exists('../data/install.lock')){
    header("location:../");
}
define('RootDir',str_replace("\\",'/',substr(__FILE__,0,strrpos(dirname(__FILE__),DIRECTORY_SEPARATOR)))); //获取网站目录
define('RootUrl',strlen(RootDir)==strlen($_SERVER['DOCUMENT_ROOT']) ? 'http://'.$_SERVER['HTTP_HOST'] : 'http://'.$_SERVER['HTTP_HOST'].substr(RootDir,strlen($_SERVER['DOCUMENT_ROOT'])));  //获取网站url
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>易讯网络PHP系列软件产品-安装向导</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="main">
  <div class="top">
    <div class="logo"></div>
    <div id="tool">联系我们&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.easyes.com.cn">  官方网站</a></div>
  </div>
  <div class="central">
    <div id="left">
     <ul>
       <li>
           <h1>1</h1>
           <div class="left_title">  
               <h2>准备安装</h2>
               <p>欢迎您使用易讯网络PHP系列软件产品！</p>
           </div>
       </li>
   <li>
           <h1>2</h1>
         <div class="left_title">  
         <h2>阅读协议</h2>
         <p>请认真阅读软件使用协议，以免您的利益受到损害！</p>
         </div>
       </li>
       <li>
           <h1 class="install">3</h1>
         <div class="left_title">  
         <h2 class="install">基本设置</h2>
         <p class="install">请设置软件的基本信息进行网站安装！</p>
         </div>
       </li>
       <li>
          <h1>4</h1>
         <div class="left_title">  
         <h2>开始安装</h2>
         <p>开始愉快的软件安装之旅吧！</p>
         </div>
       </li>
     </ul>
    </div>
    <div class="right">
      <div class="right_title">软件基本设置</div>
    
    <div style="text-align:left; line-height:25px; margin-top:20px; font-size:14px;">
<form action="install.php" method="post" name="form" id="form">
 <table class="data_set">
                  <tr><th colspan="3"></th></tr>
                  <tr>
                    <td width="14%">数据库类型</td>
                    <td width="37%"><input type="text" class="setup_input" name="DB_TYPE" value="mysql"  readonly="readonly" /></td>
                    <td width="49%" class="lightcolor">数据库类型，不需要修改 </td>
                  </tr>
                  <tr>
                   <tr><th colspan="3"></th></tr>
                    <td width="14%">数据库主机</td>
                    <td width="37%"><input type="text" class="setup_input" name="DB_HOST" value="localhost" /></td>
                    <td width="49%" class="lightcolor">数据库服务器地址，一般为localhost </td>
                  </tr>
                  <tr><th colspan="3"></th></tr>
                  <tr>
                   <tr><th colspan="3"></th></tr>
                    <td width="14%">数据库端口</td>
                    <td width="37%"><input type="text" class="setup_input" name="DB_PORT" value="3306" /></td>
                    <td width="49%" class="lightcolor">数据库端口,一般不需要修改,默认为3306</td>
                  </tr>
                  <tr><th colspan="3"></th></tr>
                  <tr>
                    <td>数据库名称</td>
                    <td><input type="text" class="setup_input" name="DB_NAME" value="vipuser" /></td>
                    <td class="lightcolor">数据库不存在，则自动创建</td>
                  </tr>
                  <tr><th height="13" colspan="3"></th></tr>
                  <tr>
                    <td>数据库用户名</td>
                    <td><input type="text" class="setup_input" name="DB_USER" value="root" /></td>
                    <td class="lightcolor">您的MySQL 用户名 </td>
                  </tr>
                  <tr><th colspan="3"></th></tr>
                  <tr>
                    <td>数据库密码</td>
                    <td><input type="password" class="setup_input" name="DB_PWD" value="" /></td>
                    <td class="lightcolor">您的MySQL密码</td>
                  </tr>
                  <tr><th colspan="3"></th></tr>
                  <tr>
                    <td>数据表前缀</td>
                    <td><input type="text" class="setup_input" name="DB_PREFIX" value="vip_" readonly="readonly" /></td>
                    <td class="lightcolor">
                    	同一数据库安装多个程序请设置前缀
                    </td>
                  </tr>
                  <tr><th colspan="3"></th></tr>
      			</table>



         
         <div class="agree"  align="center">
            <input name="web_url" type="hidden" value="<?php echo RootUrl."/" ?>" />
     
         	<input hidefocus="true" type="submit" style="margin-top:20px;" class="button" value="马上开始安装！" />

                 </div></form>

      	
     </div>
     
    </div>
  </div>
</div>
<div class="foot"></div>
</body>
</html>