<?php
//定义CanPHP框架目录
require 'config.php';
define('WEB_PATH',dirname(__FILE__));//注意目录后面加“/”
define('CP_PATH',RootDir.'/include/');
require RootDir.'/config.php';//加载配置
require CP_PATH.'core/cpApp.class.php';//加载应用控制类

//模块配置
$config['MODULE_PATH']=dirname(__FILE__).'/module/';//模块存放目录，一般不需要修改
//数据库缓存
$config['DB_CACHE_ON']=false;//后台不生成数据库缓存
$config['HTML_CACHE_ON']=false;//后台不生成静态页面
$config['URL_REWRITE_ON']=false;//是否开启重写，true开启重写,false关闭重写
//模板配置
$config['TPL_TEMPLATE_PATH']='./template/';//模板目录，一般不需要修改
$config['TPL_TEMPLATE_SUFFIX']='.html';//模板后缀，一般不需要修改，一般不需要修改
$config['TPL_CACHE_ON']=false;//是否开启模板缓存，true开启,false不开启
$config['TPL_CACHE_PATH']='./data/tpl_cache/';//模板缓存目录，一般不需要修改
$config['TPL_CACHE_SUFFIX']='.php';//模板缓存后缀,一般不需要修改，一般不需要修改

$app=new cpApp($config);//实例化单一入口应用控制类
if(URL_MODEL == 'PATHINFO') $str = strtr($str,'?=&','///');
//执行项目
$app->run();
?>