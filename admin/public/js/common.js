//Jquery和dialog弹窗插件
function look(url,name){
    var w =arguments[2]?arguments[2]:800
    var h =arguments[3]?arguments[3]:400
    art.dialog.open(url,{
        title:name,
        lock:true,
        drag:false,
        width:w,
        height:h
    });
}
//jquery全选插件
$.fn.check = function(mode){
    var mode = mode || 'on'; //default
    return this.each(function(){
        switch(mode){
            case 'on':
                this.checked = true;
                break;
            case 'off':
                this.checked = false;
                break;
            case 'toggle':
                this.checked = !this.checked;
                break;
        }
    });
};
//Jquery全选按钮
$(function(){
    $("#all").click(function(){
        if(this.checked == true){
            $("input[cssrain='one']").check('on');  
        }else{
            $("input[cssrain='one']").check('off');  
        }
    })
    $("#checkall1").click(function(){
        $("input[cssrain='one']").check('on');
    })
    $("#checkoff1").click(function(){
        $("input[cssrain='one']").check('off')
    })
    $("#checktog1").click(function(){
        $("input[cssrain='one']").check('toggle')
    })
})
/**
 * 全选checkbox,注意：标识checkbox id固定为为check_box
 * @param string name 列表check名称,如 uid[]
 */
function selectall(name) {
    if ($("#check_box").attr("checked")==false) {
        $("input[name='"+name+"']").each(function() {
            this.checked=false;
        });
    } else {
        $("input[name='"+name+"']").each(function() {
            this.checked=true;
        });
    }
}
//鼠标经过
document.onmouseover=ButtonOnMouseOver
document.onmouseout=ButtonOnMouseOut
function ButtonOnMouseOver(){
  try{
    if ((event.srcElement.type=="button")||(event.srcElement.type=="submit")||(event.srcElement.type=="reset"))
    {
      switch(event.srcElement.className)
      {
        case "button01-out" :
          event.srcElement.className="button01-over"
          break
        case "button02-out" :
          event.srcElement.className="button02-over"
          break
      }
    }
  }catch(exception){}
}
//鼠标离开
function ButtonOnMouseOut(){
  try{
    if ((event.srcElement.type=="button")||(event.srcElement.type=="submit")||(event.srcElement.type=="reset"))
    {
      switch(event.srcElement.className)
      {
        case "button01-over" :
          event.srcElement.className="button01-out"
          break
        case "button02-over" :
          event.srcElement.className="button02-out"
          break
      }
    }
  }catch(exception){}
}