var ajax=new AJAXRequest();  //实例化AJAX操作类
//POST提交
//###################################
$(function(){
    $("input").bind('keyup',function(event){
        if(event.keyCode==13){
            $(".save_ok").click();
        }
    });
    $(".save_ok").click(function(){
        var uid = $(this).attr("uid");
        ajax.postf(
        "myform",
        function(obj){
            var str = obj.responseText;
            post_json(str);
        },
        uid,
        "POST");
    })
})
//Get提交
$(function(){$(".del").click(function(){
    var uid = $(this).attr('uid');
    var __this = this;
    art.dialog.confirm('你确定执行本次操作？',function(){
        ajax.get(//开始AJAX处理
            uid+"?action=del",
            function (obj){
                var str = obj.responseText;
                if(str != 1){
                    art.dialog.alert(str);
                    return;
                }
                $(__this).parents('tr').remove();
                $(__this).parent().remove();
            }
        ) 
    });  
})})
//Get操作
$(function(){$(".goto").click(function(){
    var uid = $(this).attr('uid');
    var __this = this;
     ajax.get(//开始AJAX处理
            uid+"?action=goto",
            function (obj){
                var str = obj.responseText;
                $(__this).html(str);
           }
        ) 
})})
//把数组转换成json数据然后处理函数
//###################################
function post_json(str){
    var str = str;
    var json = jQuery.parseJSON(str);
    var act = json.act;	//操作动作 0弹出窗口,1不弹出直接返回值,2不弹出跳到指定的路径,3跳出窗口并跳到指定路径
    var uid = json.uid;	//jquery选择器属性
    var msg = json.msg;	//提示信息
    var url = json.url;	//跳转URL
    var throughBox = art.dialog.through;
    if(act == "0"){
        art.dialog.alert(msg);
    }else if(act == "1"){
        $("."+uid).html(msg);
    }else if(act == "2"){
        window.location.href = url;	
    }else if(act == "3"){
        throughBox({
            id: "myID",title:"信息提示！3秒后自动关闭",content:msg,
            icon: "face-smile",time: 3,fixed:true,cancelVal:"确定",cancel: function(){window.location.href = url;}
        });
    }
}