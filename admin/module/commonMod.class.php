<?php
//公共类
class commonMod {

    public $model; //数据库模型对象
    public $tpl; //模板对象
    public $config; //全局配置
    static $global; //静态变量，用来实现单例模式

    public function __construct() {
        date_default_timezone_set('Asia/Shanghai');  //配置系统的时区
        header("Centent-Type:text/html;charset=UTF-8");   //设置系统编码格式
        session_start(); //开启session
        //参数配置
        if (!isset(self::$global['config'])) {
            global $config;
            self::$global['config'] = $config;
        }
        $this->config = self::$global['config']; //配置
        //数据库模型初始化
        if (!isset(self::$global['model'])) {
            self::$global['model'] = new cpModel($this->config); //实例化数据库模型类
        }
        $this->model = self::$global['model']; //数据库模型对象
        //模板初始化
        if (!isset(self::$global['tpl'])) {
            self::$global['tpl'] = new cpTemplate($this->config); //实例化模板类		
        }
        $this->tpl = self::$global['tpl']; //模板类对象
        $this->checkLogin(); //检查是否登录
    }
    
    //模板变量解析
    protected function assign($name, $value) {
        return $this->tpl->assign($name, $value);
    }

    //模板输出
    protected function display($tpl = '') {
        return $this->tpl->display($tpl);
    }
    //直接跳转
    protected function redirect($url) {
        header('location:' . $url, false, 301);
        exit;
    }
    //返回关闭窗口刷新父页
    protected function closewindow(){
        $this->assign('info', $info);
        $this->display('public/closewindow');
    }
    //检查是否登录,如果还没有登录，且当前操作不是登录或者验证码生成操作，则跳到登录页面
    protected function checkLogin() {
        //登录认证
        $config['AUTH_LOGIN_URL'] = __APP__ . '/index/login.html';  //登录页面地址
        $config['AUTH_LOGIN_NO'] = array('index' => array('login'), 'common' => '*');  //不需要认证的模块和操作 
        $config['AUTH_SESSION_PREFIX'] = 'CanPHPVip_'; //认证session前缀 
        $config['AUTH_POWER_CACHE'] = false;  //是否缓存权限信息，设置false,每次从数据库读取，开发时建议设置为false
        $config['AUTH_TABLE'] = array(
            'group' => array('name' => 'group',
                'field' => array('id' => 'id', 'power' => 'power_value'),
                ),
            'resource' => array(
                'name' => 'resource', 
                'field' => array('id' => 'id', 'pid' => 'pid', 'operate' => 'operate'),
                ),
            ); //数据库表和字段映射     //数据库表和字段映射
        Auth::check($this->model, $config); //检查是否登录
    }
    //文件上传
    protected function imgupload($upload_dir) {    //imgupload是文件上传的方法名
        $upload = new UploadFile();  //实例化UploadFile();
        //以下是一些上传文件的参数设置
        //设置上传文件大小
        $upload->maxSize = 1024 * 1024 * 2;  //限制上传文件最大2M
        //设置上传文件类型
        $upload->allowExts = explode(',', 'jpg,gif,png,bmp');  //限制上传的文件的类型，可根据您要上传的文件类型进行修改
        //设置附件上传目录
        $upload->savePath = '../upload/' . $upload_dir . "/"; //设置文件上传目录的路径
        $upload->saveRule = cp_uniqid();   //设置文件上传保存的文件名，为空则以本地的文件名原名保存
        //以下没什么好说的，就是判断上传文件的状态
        if ($upload->upload()) {
            //取得成功上传的文件信息
            return $upload->getUploadFileInfo();
        }
    }
}