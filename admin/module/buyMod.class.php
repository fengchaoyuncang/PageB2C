<?php
class buyMod extends commonMod {
    public function __construct() {
        parent::__construct();
    }
    /*     * *************************************
     * 商品管理方法集
     * ***************************************** */
    //方法:商品分类列表
    //参数:无
    public function index() {
        $act = $_REQUEST['action'];
        if ($act == "alldel") {//批量删除
            $fid = $_POST['fid'];
            if (is_array($fid)) {
                $con['classid'] = 1;
                foreach ($fid as $value) {//开始删除
                    $condition['id'] = $value;
                    $this->model->table('product')->where($condition)->delete();
                    $con['pid'] = $fid;
                    $this->model->table('comment')->where($con)->delete();
                }
                Error::show('已成功删除！自动返回....', 0, __URL__ . '/index');
            } else {
                Error::show('没有选择任何要删除的资源', 1);
            }  
        }elseif($act == "del"){//单条删除
            $fid = in($_GET[0]);
            $condition['id'] = $fid;
            $result = $this->model->table('product')->where($condition)->delete();
            $con['classid'] = 1;
            $con['pid'] = $fid;
            $this->model->table('comment')->where($con)->delete();
            echo 1;
        }else{//显示内容列表
            if($_GET['search']){
                $keyword = '&'.$_SERVER['QUERY_STRING'];
                $where =' and name LIKE \'%' . in($_GET['keyword']) . '%\'';
                $this->assign('keyword', $_GET['keyword']);
            }
            $cat_id = intval(in($_GET[0])); //分类ID
            //分类
            $condition['pid'] = $cat_id;
            $class = $this->model->field('id,pid,name')->table('class')->where($condition)->select();
            $this->assign('class', $class);
            unset($condition);
            if (!empty($cat_id)) {
                $where .= ' and classid in(' . $cat_id . $this->AllChildClass($cat_id) . ')';
                $url = __URL__ . '/index-' . $cat_id . '-{page}.html'.$keyword;
            }
            //分页开始
            $url = __URL__ . '/index-0-{page}.html'.$keyword;
            $listRows = 20; //每页显示的信息条数 
            $page = new Page();
            $cur_page = $page->getCurPage($url);
            $limit_start = ($cur_page - 1) * $listRows;
            $limit = $limit_start . ',' . $listRows;
            //获取总行数
            $allcount = $this->model->table('product')->where('1' . $where)->count();
            $this->assign('allcount', $allcount);
            //读取数据开始
            $info = $this->model->table('product')->where('1' . $where)->limit($limit)->select();
            $this->assign('page', $page->show($url, $allcount, $listRows, 10, 4));
            $this->assign('info', $info);
            $this->assign('path', $this->flpath($cat_id, '/index'));
            $this->display('buy/index');    
        }
    }
    //阅读查看
    public function read(){
        $fid = in($_GET[0]); //读取用户
        $condition['id'] = intval($fid);
        $info = $this->model->table('product')->where($condition)->find();
        $this->assign('info', $info);
        $this->display('buy/read');
    }
    //管理员编辑
    public function edit() {
        $action = $_POST['action'];
        $close = in($_GET[close]);
        if ($action == 'post') {
            $fid = $_POST['fid'];
            $msg = Check::rule(
                    array(check::must($_POST['classid']), '产品分类没有选择'),
                    array(check::must($_POST['name']), '产品名称必须填写'),
                    array(check::must($_POST['marketprice']), '市场价格必须填写'),
                    array(check::rmb($_POST['marketprice']), '市场价格格式错误'),
                    array(check::must($_POST['userprice']), '优惠价格必须填写'),
                    array(check::rmb($_POST['userprice']), '优惠价格格式错误'),
                    array(check::must($_POST['vipprice']), '换购积分必须填写'),
                    array(check::num($_POST['vipprice']), '换购积分只能是整数'),
                    array(check::must($_POST['downdate']), '下架日期必须填写')
             );
            if ($msg !== true) {
                Error::show($msg, 1);
            }
            if ($info == $this->imgupload('shop')) {//实例化
                $_POST['images'] = $info[0]['savename'];   //将文件保存的文件名插入数据'imgurl'库字段
            }
            $_POST['downdate'] = strtotime($_POST['downdate']);
            $data = postinput($_POST);
            if ($fid) {
                $condition['id'] = $fid;
                $result = $this->model->table('product')->data($data)->where($condition)->update();
            } else {
                $result = $this->model->table('product')->data($data)->insert();
            }
            Error::show('已成功提交！', 0, __URL__ . '/index-'.$_POST['classid'].'.html');
        } else {
            $fid = in($_GET[0]); //读取用户
            $condition['id'] = intval($fid);
            $info = $this->model->table('product')->where($condition)->find();
            $this->assign('info', $info);
            $this->assign('class', $this->getClass());
            $this->assign('special', $this->getspecial());
            $this->display('buy/edit');
        }
    }
  //方法:评论
    //参数:无
    public function comment(){
        $fid = in($_GET[0]); //读取用户
        $act = $_GET['action'];
        if ($act == "goto") {
            $condition['id'] = $fid;
            $info = $this->model->table('product')->where($condition)->field('comment')->find();
            $data = array();
            if($info['comment'] == 1){
               $data['comment'] = 2; 
            }else{
               $data['comment'] = 1; 
            }
            $this->model->table('product')->data($data)->where($condition)->update();
            if($data['comment'] == 1){
                echo '<span class="hot">√</span>';
            }else{
                echo '×';
            }
        }
    }
    //方法:审核
    //参数:无
    public function pass(){
        $fid = in($_GET[0]); //读取用户
        $act = $_GET['action'];
        if ($act == "goto") {
            $condition['id'] = $fid;
            $info = $this->model->table('product')->where($condition)->field('pass')->find();
            $data = array();
            if($info['pass'] == 1){
               $data['pass'] = 2; 
            }else{
               $data['pass'] = 1; 
            }
            $this->model->table('product')->data($data)->where($condition)->update();
            if($data['pass'] == 1){
                echo '<span class="hot">√</span>';
            }else{
                echo '×';
            }
        }
    }
    /*     * *************************************
     * 商品分类方法集
     * ***************************************** */
    
    //方法:商品分类列表
    //参数:无
    public function buyclass(){
        $action = $_REQUEST['action'];
        if ($action == 'alldel') {
            $fid = $_POST['fid'];
            if (is_array($fid)) {
                foreach ($fid as $value) {
                    $result = $this->classdel($value);
                    if($result){
                        Error::show('删除失败,分类中还有数据.', 1);
                    }
                    //开始删除
                    $condition['id'] = $value;
                    $this->model->table('class')->where($condition)->delete();
                }
                Error::show('已成功删除！自动返回....', 0, __URL__ . '/buyclass');
            } else {
                Error::show('没有任何操作', 1);
            }
        }elseif($action == 'del'){
            $fid = in($_GET[0]); //读取用户
            $result = $this->classdel($fid);
            if ($result) {
                echo "删除失败，序号为:[$fid]的分类中还有数据";
            }else{
                $condition['id'] = $fid;
                $result = $this->model->table('class')->where($condition)->delete();
                echo 1;
            }
        }else {
            $fid = in($_GET[0]);
            $condition['pid'] = intval($fid);
            $this->assign('path', $this->flpath($fid, '/buyclass'));
            $info = $this->model->table('class')->where($condition)->select();
            $this->assign('fid', $fid);
            $this->assign('info', $info);
            $this->display('buy/class');
        }
    }
    private function classdel($value){
        //判断是否有数据
        $arrlist['classid'] = $value;
        $info1 = $this->model->table('product')->where($arrlist)->find();
        if ($info1) {
            return TRUE;
        }
        //判断是否有子分类
        $arrclass['pid'] = $value;
        $info2 = $this->model->table('class')->where($arrclass)->find();
        if ($info2) {
            return TRUE;
        }
        return FALSE;
    }
    //方法:商品分类编辑
    //参数:无
    public function classedit(){
        $action = $_POST['action'];
        $close = in($_GET[close]);
        if ($action == 'post') {
            $fid = in($_POST['fid']);
            $_POST['update'] = time();
            $data = postinput($_POST);
            if ($fid) {//更新
                $condition['id'] = $fid;
                $result = $this->model->table('class')->data($data)->where($condition)->update();
            } else {//添加
                $result = $this->model->table('class')->data($data)->insert();
            }
            Error::show('已成功提交', 0, $this->closewindow());
        } else {
            $fid = in($_GET[0]);
            $pid = in($_GET[1]);
            $arr['id'] = intval($fid);
            $info = $this->model->table('class')->where($arr)->find();
            $this->assign('path', $this->flpath($pid, '/buyclass'));
            $this->assign('info', $info);
            $this->assign('pid', $pid);
            $this->assign('close', $close);
            $this->display('buy/classedit');
        } 
    }
    //方法：递归搜当前目录下所有的下级目录（无限层型目录）
    //参数：栏目ID
    protected function AllChildClass($pid = 0) {
        $pid = intval($pid);
        $sql = $this->getClass($pid);
        foreach ($sql as $pid) {
            $str .= "," . $pid['id'];
        }
        return $str;
    }
    //方法:获取分类树， 获取ID包含所有分类结构树
    //参数:父栏目ID
    public function getClass($id=0) {
        //查询分类信息
        $data = $this->model->field('id,pid,name')->table('class')->select();
        $cat = new Category(array('id', 'pid', 'name', 'cname')); //初始化无限分类
        return $cat->getTree($data, $id); //获取分类数据树结构
    }
    //方法:当前分类的位置
    //参数：pid栏目ID,action当前位置栏目连接页面
    protected function flpath($pid = 0, $action) {
        $condition['id'] = intval($pid);
        $pname = $this->model->table('class')->field('id,pid,name')->where($condition)->find();
        if ($pname) {
            $Str = $this->flpath($pname['pid'], $action);
            $Str .= '<a href="' . __URL__ . $action . '-' . $pname['id'] . '.html' . '" target="right">' . $pname['name'] . '</a> >> ';
        }
        return $Str;
    }
    /*     * *************************************
     * 商品特性方法集
     * ***************************************** */
    
    //方法:商品特性列表
    //参数:无
    public function special() {
        $action = $_REQUEST['action'];
        if ($action == 'alldel') {
            $fid = $_POST['fid'];
            if (is_array($fid)) {
                foreach ($fid as $value) {
                    $result = $this->classdel($value);
                    if ($result) {
                        Error::show('删除失败,分类中还有数据.', 1);
                    }
                    //开始删除
                    $condition['id'] = $value;
                    $this->model->table('special')->where($condition)->delete();
                }
                Error::show('已成功删除！自动返回....', 0, __URL__ . '/special');
            } else {
                Error::show('没有任何操作', 1);
            }
        } elseif ($action == 'del') {
            $fid = in($_GET[0]); //读取用户
            $result = $this->classdel($fid);
            if ($result) {
                echo "删除失败，序号为:[$fid]的分类中还有数据";
            } else {
                $condition['id'] = $fid;
                $result = $this->model->table('special')->where($condition)->delete();
                echo 1;
            }
        } else {
            $fid = in($_GET[0]);
            $condition['pid'] = intval($fid);
            $this->assign('path', $this->sppath($fid, '/special'));
            $info = $this->model->table('special')->where($condition)->select();
            $this->assign('fid', $fid);
            $this->assign('info', $info);
            $this->display('buy/special');
        }
    }
    //方法:商品特性编辑
    //参数:无
    public function specialedit() {
        $action = $_POST['action'];
        $close = in($_GET[close]);
        if ($action == 'post') {
            $fid = in($_POST['fid']);
            $_POST['update'] = time();
            $data = postinput($_POST);
            if ($fid) {//更新
                $condition['id'] = $fid;
                $result = $this->model->table('special')->data($data)->where($condition)->update();
            } else {//添加
                $result = $this->model->table('special')->data($data)->insert();
            }
            Error::show('已成功提交', 0, $this->closewindow());
        } else {
            $fid = in($_GET[0]);
            $pid = in($_GET[1]);
            if ($fid) {
                $this->assign('path', $this->flpath($fid, '/special'));
            } else {
                $this->assign('path', $this->flpath($pid, '/special'));
            }
            $arr['id'] = intval($fid);
            $info = $this->model->table('special')->where($arr)->find();
            $this->assign('info', $info);
            $this->assign('pid', $pid);
            $this->assign('close', $close);
            $this->display('buy/specialedit');
        }
    }
    //方法:获取分类树， 获取ID包含所有分类结构树
    //参数:父栏目ID
    public function getspecial($id=0) {
        //查询分类信息
        $data = $this->model->field('id,pid,name')->table('special')->select();
        $cat = new Category(array('id', 'pid', 'name', 'cname')); //初始化无限分类
        return $cat->getTree($data, $id); //获取分类数据树结构
    }
    //方法:当前特性的位置
    //参数：pid栏目ID,action当前位置栏目连接页面
    protected function sppath($pid = 0, $action) {
        $condition['id'] = intval($pid);
        $pname = $this->model->table('special')->field('id,pid,name')->where($condition)->find();
        if ($pname) {
            $Str = $this->sppath($pname['pid'], $action);
            $Str .= '<a href="' . __URL__ . $action . '-' . $pname['id'] . '.html' . '" target="right">' . $pname['name'] . '</a> >> ';
        }
        return $Str;
    }
}