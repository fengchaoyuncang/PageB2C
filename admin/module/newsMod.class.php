<?php
class newsMod extends commonMod {
    public function __construct() {
        parent::__construct();
    }
    /*     * *************************************
     * 新闻管理方法集
     * ***************************************** */
    //方法:新闻分类列表
    //参数:无
    public function index() {
        $act = $_REQUEST['action'];
        if ($act == "alldel") {//批量删除
            $fid = $_POST['fid'];
            if (is_array($fid)) {
                $con['classid'] = 2;
                foreach ($fid as $value) {//开始删除
                    $condition['id'] = in($value);
                    $this->model->table('news')->where($condition)->delete();
                    $con['pid'] = $fid;
                    $this->model->table('comment')->where($con)->delete();
                }
                Error::show('已成功删除！自动返回....', 0, __URL__ . '/index');
            } else {
                Error::show('没有选择任何要删除的资源', 1);
            }
        }elseif($act == "del"){//单条删除
            $fid = in($_GET[0]);
            $condition['id'] = $fid;
            $this->model->table('news')->where($condition)->delete();
            $con['classid'] = 2;
            $con['pid'] = $fid;
            $this->model->table('comment')->where($con)->delete();
            echo 1;
        }else{//显示内容列表
            $cat_id = intval(in($_GET[0])); //分类ID
            //分类
            $condition['pid'] = $cat_id;
            $class = $this->model->field('id,pid,name')->table('newsclass')->where($condition)->select();
            $this->assign('class', $class);
            unset($condition);
            if (!empty($cat_id)) {
                $where = ' and classid in(' . $cat_id . $this->AllChildClass($cat_id) . ')';
                $url = __URL__ . '/index-' . $cat_id . '-{page}.html';
            }
            //分页开始
            $url = __URL__ . '/index-0-{page}.html';
            $listRows = 20; //每页显示的信息条数 
            $page = new Page();
            $cur_page = $page->getCurPage($url);
            $limit_start = ($cur_page - 1) * $listRows;
            $limit = $limit_start . ',' . $listRows;
            //获取总行数
            $allcount = $this->model->table('news')->where('1' . $where)->count();
            $this->assign('allcount', $allcount);
            //读取数据开始
            $info = $this->model->table('news')->where('1' . $where)->limit($limit)->select();
            $this->assign('page', $page->show($url, $allcount, $listRows, 10, 4));
            $this->assign('info', $info);
            $this->assign('path', $this->flpath($cat_id, '/index'));
            $this->display('news/index');    
        }
    }
    //阅读查看
    public function read(){
        $fid = in($_GET[0]); //读取用户
        $condition['id'] = intval($fid);
        $info = $this->model->table('news')->where($condition)->find();
        $this->assign('info', $info);
        $this->display('news/read');
    }
    //管理员编辑
    public function edit() {
        $action = $_POST['action'];
        $close = in($_GET[close]);
        if ($action == 'post') {
            $fid = $_POST['fid'];
            $msg = Check::rule(
                    array(check::must($_POST['classid']), '产品分类没有选择'),
                    array(check::must($_POST['title']), '产品名称必须填写')
             );
            if ($msg !== true) {
                Error::show($msg, 1);
            }
            if ($info = $this->imgupload('news')) {//实例化
                $_POST['images'] = $info[0]['savename'];   //将文件保存的文件名插入数据'imgurl'库字段
            }
            $_POST['uptime'] = time();
            $data = postinput($_POST);
            if ($fid) {
                $condition['id'] = $fid;
                $result = $this->model->table('news')->data($data)->where($condition)->update();
            } else {
                $result = $this->model->table('news')->data($data)->insert();
            }
            Error::show('已成功提交！', 0, __URL__ . '/index-'.$_POST['classid'].'.html');
        } else {
            $fid = in($_GET[0]); //读取用户
            $condition['id'] = intval($fid);
            $info = $this->model->table('news')->where($condition)->find();
            $this->assign('info', $info);
            $this->assign('class', $this->getClass());
            $this->display('news/edit');
        }
    }
    //方法:评论
    //参数:无
    public function comment(){
        $fid = in($_GET[0]); //读取用户
        $act = $_GET['action'];
        if ($act == "goto") {
            $condition['id'] = $fid;
            $info = $this->model->table('news')->where($condition)->field('comment')->find();
            $data = array();
            if($info['comment'] == 1){
               $data['comment'] = 2; 
            }else{
               $data['comment'] = 1; 
            }
            $this->model->table('news')->data($data)->where($condition)->update();
            if($data['comment'] == 1){
                echo '<span class="hot">√</span>';
            }else{
                echo '×';
            }
        }
    }
    //方法:审核
    //参数:无
    public function pass(){
        $fid = in($_GET[0]); //读取用户
        $act = $_GET['action'];
        if ($act == "goto") {
            $condition['id'] = $fid;
            $info = $this->model->table('news')->where($condition)->field('pass')->find();
            $data = array();
            if($info['pass'] == 1){
               $data['pass'] = 2; 
            }else{
               $data['pass'] = 1; 
            }
            $this->model->table('news')->data($data)->where($condition)->update();
            if($data['pass'] == 1){
                echo '<span class="hot">√</span>';
            }else{
                echo '×';
            }
        }
    }
    //方法:新
    //参数:无
    public function news(){
        $fid = in($_GET[0]); //读取用户
        $act = $_GET['action'];
        if ($act == "goto") {
            $condition['id'] = $fid;
            $info = $this->model->table('news')->where($condition)->field('new')->find();
            $data = array();
            if($info['new'] == 1){
               $data['new'] = 2; 
            }else{
               $data['new'] = 1; 
            }
            $this->model->table('news')->data($data)->where($condition)->update();
            if($data['new'] == 1){
                echo '<span class="hot">√</span>';
            }else{
                echo '×';
            }
        }
    }
    //方法:定
    //参数:无
    public function top(){
        $fid = in($_GET[0]); //读取用户
        $act = $_GET['action'];
        if ($act == "goto") {
            $condition['id'] = $fid;
            $info = $this->model->table('news')->where($condition)->field('top')->find();
            $data = array();
            if($info['top'] == 1){
               $data['top'] = 2; 
            }else{
               $data['top'] = 1; 
            }
            $this->model->table('news')->data($data)->where($condition)->update();
            if($data['top'] == 1){
                echo '<span class="hot">√</span>';
            }else{
                echo '×';
            }
        }
    }
    //方法:推荐
    //参数:无
    public function best(){
        $fid = in($_GET[0]); //读取用户
        $act = $_GET['action'];
        if ($act == "goto") {
            $condition['id'] = $fid;
            $info = $this->model->table('news')->where($condition)->field('best')->find();
            $data = array();
            if($info['best'] == 1){
               $data['best'] = 2; 
            }else{
               $data['best'] = 1; 
            }
            $this->model->table('news')->data($data)->where($condition)->update();
            if($data['best'] == 1){
                echo '<span class="hot">√</span>';
            }else{
                echo '×';
            }
        }
    } 
    /*     * *************************************
     * 新闻分类方法集
     * ***************************************** */
    //方法:新闻分类列表
    //参数:无
    public function newsclass(){
        $action = $_REQUEST['action'];
        if ($action == 'alldel') {
            $fid = $_POST['fid'];
            if (is_array($fid)) {
                foreach ($fid as $value) {
                    $result = $this->classdel($value);
                    if($result){
                        Error::show('删除失败,分类中还有数据.', 1);
                    }
                    //开始删除
                    $condition['id'] = $value;
                    $this->model->table('newsclass')->where($condition)->delete();
                }
                Error::show('已成功删除！自动返回....', 0, __URL__ . '/newsclass');
            } else {
                Error::show('没有任何操作', 1);
            }
        }elseif($action == 'del'){
            $fid = in($_GET[0]); //读取用户
            $result = $this->classdel($fid);
            if ($result) {
                echo "删除失败，序号为:[$fid]的分类中还有数据";
            }else{
                $condition['id'] = $fid;
                $result = $this->model->table('newsclass')->where($condition)->delete();
                echo 1;
            }
        }else {
            $fid = in($_GET[0]);
            $condition['pid'] = intval($fid);
            $this->assign('path', $this->flpath($fid, '/newsclass'));
            $info = $this->model->table('newsclass')->where($condition)->select();
            $this->assign('fid', $fid);
            $this->assign('info', $info);
            $this->display('news/class');
        }
    }
    private function classdel($value){
        //判断是否有数据
        $arrlist['classid'] = $value;
        $info1 = $this->model->table('news')->where($arrlist)->find();
        if ($info1) {
            return TRUE;
        }
        //判断是否有子分类
        $arrclass['pid'] = $value;
        $info2 = $this->model->table('newsclass')->where($arrclass)->find();
        if ($info2) {
            return TRUE;
        }
        return FALSE;
    }
    //方法:新闻分类编辑
    //参数:无
    public function classedit(){
        $action = $_POST['action'];
        $close = in($_GET[close]);
        if ($action == 'post') {
            $fid = in($_POST['fid']);
            $_POST['update'] = time();
            $data = postinput($_POST);
            if ($fid) {//更新
                $condition['id'] = $fid;
                $result = $this->model->table('newsclass')->data($data)->where($condition)->update();
            } else {//添加
                $result = $this->model->table('newsclass')->data($data)->insert();
            }
            Error::show('已成功提交', 0, $this->closewindow());
        } else {
            $fid = in($_GET[0]);
            $pid = in($_GET[1]);
            $arr['id'] = intval($fid);
            $info = $this->model->table('newsclass')->where($arr)->find();
            $this->assign('path', $this->flpath($pid, '/newsclass'));
            $this->assign('info', $info);
            $this->assign('pid', $pid);
            $this->assign('close', $close);
            $this->display('buy/classedit');
        } 
    }
    //方法：递归搜当前目录下所有的下级目录（无限层型目录）
    //参数：栏目ID
    protected function AllChildClass($pid = 0) {
        $pid = intval($pid);
        $sql = $this->getClass($pid);
        foreach ($sql as $pid) {
            $str .= "," . $pid['id'];
        }
        return $str;
    }
    //方法:获取分类树， 获取ID包含所有分类结构树
    //参数:父栏目ID
    public function getClass($id=0) {
        //查询分类信息
        $data = $this->model->field('id,pid,name')->table('newsclass')->select();
        $cat = new Category(array('id', 'pid', 'name', 'cname')); //初始化无限分类
        return $cat->getTree($data, $id); //获取分类数据树结构
    }
    //方法:当前分类的位置
    //参数：pid栏目ID,action当前位置栏目连接页面
    protected function flpath($pid = 0, $action) {
        $condition['id'] = intval($pid);
        $pname = $this->model->table('newsclass')->field('id,pid,name')->where($condition)->find();
        if ($pname) {
            $Str = $this->flpath($pname['pid'], $action);
            $Str .= '<a href="' . __URL__ . $action . '-' . $pname['id'] . '.html' . '" target="right">' . $pname['name'] . '</a> >> ';
        }
        return $Str;
    }
    /*     * *************************************
     * 单页方法集
     * ***************************************** */
    //方法:单页列表
    //参数:无
    public function page(){
        $act = $_REQUEST['action'];
        if ($act == "alldel") {//批量删除
            $fid = $_POST['fid'];
            if (is_array($fid)) {
                foreach ($fid as $value) {//开始删除
                    $condition['id'] = in($value);
                    $this->model->table('page')->where($condition)->delete();
                }
                Error::show('已成功删除！自动返回....', 0, __URL__ . '/page');
            } else {
                Error::show('没有选择任何要删除的资源', 1);
            }
        }elseif($act == "del"){//单条删除
            $fid = in($_GET[0]);
            $condition['id'] = $fid;
            $result = $this->model->table('page')->where($condition)->delete();
            echo 1;
        }else{
            $info = $this->model->table('page')->where()->select();
            $this->assign('info', $info);
            $this->display('page/page');   
        }
    }
    //方法:添加编辑单页
    public function pageedit(){
        $action = $_POST['action'];
        $close = in($_GET[close]);
        if ($action == 'post') {
            $fid = $_POST['fid'];
            $msg = Check::rule(
                    array(check::must($_POST['title']), '单页名称必须填写'),
                    array(check::must($_POST['titlekey']), 'SEO副标题必须填写'),
                    array(check::must($_POST['keywords']), 'SEO关键词必须填写'),
                    array(check::must($_POST['description']), 'SEO页面说明必须填写')
             );
            if ($msg !== true) {
                Error::show($msg, 1);
            }
            if ($info = $this->imgupload('news')) {//实例化
                $_POST['images'] = $info[0]['savename'];   //将文件保存的文件名插入数据'imgurl'库字段
            }
            $_POST['uptime'] = time();
            $data = postinput($_POST);
            if ($fid) {
                $condition['id'] = $fid;
                $result = $this->model->table('page')->data($data)->where($condition)->update();
            } else {
                $result = $this->model->table('page')->data($data)->insert();
            }
            Error::show('已成功提交！', 0, __URL__ . '/page.html');
        } else {
            $fid = in($_GET[0]); //读取用户
            $condition['id'] = intval($fid);
            $info = $this->model->table('page')->where($condition)->find();
            $this->assign('info', $info);
            $this->display('page/edit');
        }
    }
    //方法:阅读查看
    public function pageread(){
        $fid = in($_GET[0]); //读取用户
        $condition['id'] = intval($fid);
        $info = $this->model->table('page')->where($condition)->find();
        $this->assign('info', $info);
        $this->display('page/read');
    }
}