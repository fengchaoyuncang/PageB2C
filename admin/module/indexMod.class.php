<?php
class indexMod extends commonMod {
    public function __construct() {
        parent::__construct();
    }
    public function index() {
        $this->assign('sitename',self::$global['config']['sitename']);
        $this->assign('key', self::$global['config']['key']);
        $this->assign('myinfo', $_SESSION['APP']);
        $this->display('index/index');
    }
    public function login() {
        $action = $_REQUEST['action'];
        if($action == "post"){//获取数据
            $username = in($_POST['admin_user']);
            $password = md5($_POST['admin_pass']);
            if (empty($username) || empty($_POST['admin_pass'])) {
                json(0, '对不起!用户名或密码没有填写！');
            }
            //数据库操作
            if ($this->_login($username, $password)) {
                json(3, '您已成功登录！载入中...！', __URL__);
            } else {
                json(0, '用户被锁定或用户名密码输入错误，建议重新登录！');
            }
        }
        $this->display('index/login');
    }
    //用户登录
    private function _login($username, $password) {
        $condition['admin_user'] = $username;
        $user = $this->model->table('admin')->where($condition)->find();
        if ($user['admin_pass'] == $password) {
            if ($user['admin_lock'] == 1) {
                return false;
            }
            //更新帐号信息
            $data['admin_ip']   = get_client_ip();
            $data['admin_date'] = $_SERVER['REQUEST_TIME'];
            $up['admin_id']     = $user['admin_id'];
            $this->model->table('admin')->data($data)->where($up)->update();
            $_SESSION['APP']['admin_id']    = $user['admin_id'];
            $_SESSION['APP']['group_id']    = $user['group_id'];
            $_SESSION['APP']['admin_user']  = $user['admin_user'];
            $_SESSION['APP']['admin_name']  = $user['admin_name'];
            $_SESSION['APP']['admin_ip']    = $user['admin_ip'];
            $_SESSION['APP']['admin_date']  = $user['admin_date'];
            $_SESSION['APP']['__ROOT__']    = __ROOT__;
            Auth::set($_SESSION['APP']['group_id']);  //设置认证组ID
            return true;
        }
    }
    //左侧菜单
    public function menuleft() {
        $menuid = intval($_GET['menuid']);
        $this->assign('menuid', $menuid);
        $this->display('index/left');
    }
    //右侧菜单
    public function menuright() {
        $condition['admin_id'] = $_SESSION['APP']['admin_id'];
        $user = $this->model->table('admin')->where($condition)->find();
        $ip=new IpArea();
        $userip = $user['admin_ip'];
        $address = $ip->get($userip);
        $this->assign('address', $address);
        $this->assign('userinfo', $user);
        $this->display('index/right');
    }
    public  function maps(){
        $address = $_GET['address'];
        $this->assign('address', $address);
        $this->display('index/maps');
    }
    //右侧菜单
    public function menu() {
	$menuid=intval($_GET['menuid']);
        $array = array(
            10=>'控制面板 > 管理员 ',11=>"控制面板 > 积分设定 ",  12 => "控制面板 > 会员等级 ",  13 => "控制面板 > 参数设置 ",  14=> "控制面板 > 管理权限 ",
            20 => '会员管理 > 会员管理 ', 21 => "会员管理 > 订单管理 ",
            30 => '商品管理 > 商品分类 ', 31 => "商品管理 > 商品特性 ", 32 => "商品管理 > 商品管理 ",
            40 => '留言评论 > 留言管理 ', 41 => "留言评论 > 评论管理 ",
            50 => '问卷调查 > 问卷调查 ', 51 => "问卷调查 > 调查结果 ",
            60 => '新闻公告 > 新闻分类 ', 61 => "新闻公告 > 新闻管理 ", 62 => "新闻公告 > 单页管理 ",
        );
        echo $array[$menuid];
        return;
    }   
//用户退出
    public function logout() {
        Auth::clear();
        session_destroy();
        unset($_SESSION['APP']);
        $this->redirect(__APP__);
    }
}
?>