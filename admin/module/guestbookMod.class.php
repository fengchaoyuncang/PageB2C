<?php
class guestbookMod extends commonMod {
    public function __construct() {
        parent::__construct();
    }
    /* ***********************
     * 留言板管理
     * ***********************/
    //留言信息
    public function index() {
        $fid = in($_GET[0]); //读取用户
        $act = $_REQUEST['action'];
        if ($act == "alldel") {//批量删除
            $fid = $_POST['fid'];
            if (is_array($fid)) {
                foreach ($fid as $value) {//开始删除
                    $condition['id'] = in($value);
                    $this->model->table('guestbook')->where($condition)->delete();
                }
                Error::show('已成功删除！自动返回....', 0, __URL__ . '/index');
            } else {
                Error::show('没有选择任何要删除的资源', 1);
            }
        }elseif ($act == "del") {//删除用户
            $condition['id'] = $fid;
            $result = $this->model->table('guestbook')->where($condition)->delete();
            if($result){
                echo 1;
            }else{
                echo "删除失败";
            }
        } else {
           if($_GET['search']){
                $keyword = '&'.$_SERVER['QUERY_STRING'];
                $starttime = strtotime($_GET['starttime']);
                $endtime   = strtotime($_GET['endtime']);
                $condition ="uptime>=".$starttime." and uptime<=".$endtime;
                $this->assign('starttime', $starttime);
                $this->assign('endtime', $endtime);
            }
            //分页开始
            $url = __URL__ . '/index-{page}.html'.$keyword;
            $listRows = 5; //每页显示的信息条数 
            $page = new Page();
            $cur_page = $page->getCurPage($url);
            $limit_start = ($cur_page - 1) * $listRows;
            $limit = $limit_start . ',' . $listRows;
            //获取总行数
            $allcount = $this->model->table('guestbook')->where($condition)->count();
            $this->assign('allcount', $allcount);
            $this->assign('page', $page->show($url, $allcount, $listRows, 10, 4));
            //读取数据开始
            $info = $this->model->table('guestbook')->where($condition)->limit($limit)->order('id desc')->select();
            $this->assign('info', $info);
            $this->display('guestbook/index');
        }
    }
    //阅读查看
    public function open(){
        $fid = in($_GET[0]); //读取用户
        $act = $_GET['action'];
        if ($act == "goto") {
            $condition['id'] = $fid;
            $info = $this->model->table('guestbook')->where($condition)->field('open')->find();
            $data = array();
            if($info['open']){
               $data['open'] = null; 
            }else{
               $data['open'] = 1; 
            }
            $this->model->table('guestbook')->data($data)->where($condition)->update();
            if($data['open']){
                echo '已审';
            }else{
                echo '<span class="hot">未审</span>';
            }
        }
    }
    //回复提交
    public function edit() {
        $action = $_POST['action'];
        if ($action == 'post') {
            $fid = $_POST['fid'];
            $_POST['replytime'] = time();
            $data = postinput($_POST);
            if ($fid) {
                $condition['id'] = $fid;
                $result = $this->model->table('guestbook')->data($data)->where($condition)->update();
            } else {
                $result = $this->model->table('guestbook')->data($data)->insert();
            }
            Error::show('回复提交成功！', 0, $this->closewindow());
        } else {
            $fid = in($_GET[0]); //读取用户
            $close = in($_GET[close]);
            $condition['id'] = intval($fid);
            $info = $this->model->table('guestbook')->where($condition)->find();
            $this->assign('info', $info);
            $this->assign('close', $close);
            $this->display('guestbook/edit');
        }
    }
    /* ***********************
     * 评论管理
     * ***********************/
    //评论信息
    public function comment(){
        $fid = in($_GET[0]); //读取用户
        $act = $_REQUEST['action'];
        if ($act == "alldel") {//批量删除
            $fid = $_POST['fid'];
            if (is_array($fid)) {
                foreach ($fid as $value) {//开始删除
                    $condition['id'] = in($value);
                    $this->model->table('comment')->where($condition)->delete();
                }
                Error::show('已成功删除！自动返回....', 0, __URL__ . '/comment');
            } else {
                Error::show('没有选择任何要删除的资源', 1);
            }
        }elseif ($act == "del") {//删除用户
            $condition['id'] = $fid;
            $result = $this->model->table('comment')->where($condition)->delete();
            if($result){
                echo 1;
            }else{
                echo "删除失败";
            }
        } else {
           if($_GET['search']){
                $keyword = '&'.$_SERVER['QUERY_STRING'];
                $starttime = strtotime($_GET['starttime']);
                $endtime   = strtotime($_GET['endtime']);
                $condition ="uptime>=".$starttime." and uptime<=".$endtime;
                $this->assign('starttime', $starttime);
                $this->assign('endtime', $endtime);
            }
            //分页开始
            $url = __URL__ . '/comment-{page}.html'.$keyword;
            $listRows = 5; //每页显示的信息条数 
            $page = new Page();
            $cur_page = $page->getCurPage($url);
            $limit_start = ($cur_page - 1) * $listRows;
            $limit = $limit_start . ',' . $listRows;
            //获取总行数
            $allcount = $this->model->table('comment')->where($condition)->count();
            $this->assign('allcount', $allcount);
            $this->assign('page', $page->show($url, $allcount, $listRows, 10, 4));
            //读取数据开始
            $info = $this->model->table('comment')->where($condition)->limit($limit)->order('id desc')->select();
            $this->assign('info', $info);
            $this->display('guestbook/comment');
        }
    }
    //阅读查看
    public function commentopen(){
        $fid = in($_GET[0]); //读取用户
        $act = $_GET['action'];
        if ($act == "goto") {
            $condition['id'] = $fid;
            $info = $this->model->table('comment')->where($condition)->field('open')->find();
            $data = array();
            if($info['open']){
               $data['open'] = null; 
            }else{
               $data['open'] = 1; 
            }
            $this->model->table('comment')->data($data)->where($condition)->update();
            if($data['open']){
                echo '已审';
            }else{
                echo '<span class="hot">未审</span>';
            }
        }
    }
    //回复提交
    public function commentedit() {
        $action = $_POST['action'];
        if ($action == 'post') {
            $fid = $_POST['fid'];
            $_POST['replytime'] = time();
            $data = postinput($_POST);
            if ($fid) {
                $condition['id'] = $fid;
                $result = $this->model->table('comment')->data($data)->where($condition)->update();
            } else {
                $result = $this->model->table('comment')->data($data)->insert();
            }
            Error::show('回复提交成功！', 0,$this->closewindow());
        } else {
            $fid = in($_GET[0]); //读取用户
            $condition['id'] = intval($fid);
            $info = $this->model->table('comment')->where($condition)->find();
            $this->assign('info', $info);
            $this->assign('close', $close);
            $this->display('guestbook/commentedit');
        }
    }
}