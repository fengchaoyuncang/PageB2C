<?php
class manageMod extends commonMod{
    public function __construct(){
        parent::__construct();
    }
    //#####################################################
    //管理员信息
    public function index(){
        $fid = in($_GET[0]); //读取用户
        $act = $_GET['action'];
        if ($act == "del") {//删除用户
            if ($fid == 1) {
                echo "超级管理员禁止删除";
                return;
            }
            $condition['admin_id'] = $fid;
            $result = $this->model->table('admin')->where($condition)->delete();
            echo 1;
        }else{
            $info = $this->model->table('admin')->where()->select();
            $this->assign('info', $info);
            $this->display('manage/index'); 
        }
    }
    //管理员编辑
    public function edit(){
        $action = $_POST['action'];
        $close = in($_GET[close]);
        if ($action == 'post') {
            $fid = $_POST['fid'];
            $admin_user = in($_POST['admin_user']);
            $admin_name = in($_POST['admin_name']);
            if ($_POST['admin_pass'])
                $_POST['admin_pass'] = md5($_POST['admin_pass']);
            $_POST['admin_date'] = time();
            $data = postinput($_POST);
            if ($fid) {
                $condition['admin_id'] = $fid;
                $result = $this->model->table('admin')->data($data)->where($condition)->update();
            } else {
                $condition['admin_user'] = $admin_user;
                $info = $this->model->table('admin')->where($condition)->find();
                if ($info) {
                    Error::show('相同用户已存在！', 1);
                }
                $result = $this->model->table('admin')->data($data)->insert();
            }
            Error::show('已成功提交！', 0, $this->closewindow());
        }else{
            $fid = in($_GET[0]); //读取用户
            $close = in($_GET[close]);
            $condition['admin_id'] = intval($fid);
            $info = $this->model->table('admin')->where($condition)->find();
            $class = $this->model->table('group')->field('id,name')->where()->select();
            $this->assign('info', $info);
            $this->assign('class', $class);
            $this->assign('close', $close);
            $this->display('manage/edit');  
        }
    }
    //#####################################################
    //积分增加
    public function vip() {
        $info = $this->model->table('sys')->where()->select();
        $this->assign('info', $info);
        $this->display('manage/vip');
    }
    //编辑积分
    public function vipedit(){
        $action = $_POST['action'];
        $close = in($_GET[close]);
        if ($action == 'post') {
            $fid = $_POST['fid'];
            $condition['id'] = intval($fid);
            $data = postinput($_POST);
            $result = $this->model->table('sys')->data($data)->where($condition)->update();
            Error::show('已成功提交！', 0, __URL__ . '/vipedit-'.$fid.'.html?close=1');
        }else {
            $fid = in($_GET[0]); //读取用户
            $condition['id'] = intval($fid);
            $info = $this->model->table('sys')->where($condition)->find();
            $this->assign('info', $info);
            $this->assign('close', $close);
            $this->display('manage/vipedit');
        }
    }
    //#####################################################
    //系统等级
    public function grade() {
        $fid = in($_GET[0]); //读取用户
        $act = $_GET['action'];
        if ($act == "del") {//删除用户
            $condition['id'] = $fid;
            $result = $this->model->table('grade')->where($condition)->delete();
            echo 1;
        }else{
            $info = $this->model->table('grade')->where()->select();
            $this->assign('info', $info);
            $this->display('manage/grade');    
        }
    }
    //编辑等级
    public function gradeedit() {
        $action = $_POST['action'];
        $close = in($_GET[close]);
        if ($action == 'post') {
            $fid = $_POST['fid'];
            $rebate = $_POST['rebate'];
            if($rebate == '0'){
                Error::show('折扣率不能填写 0', 1);
            }
            $data = postinput($_POST);
            if ($fid) {
                $condition['id'] = intval($fid);
                $result = $this->model->table('grade')->data($data)->where($condition)->update();
            } else {
                $result = $this->model->table('grade')->data($data)->insert();
            }
            Error::show('已成功提交！', 0, $this->closewindow());
        } else {
            $fid = in($_GET[0]); //读取用户
            $condition['id'] = intval($fid);
            $info = $this->model->table('grade')->where($condition)->find();
            $this->assign('info', $info);
            $this->assign('close', $close);
            $this->display('manage/gradeedit');
        }
    }
    /* ********************************
     * 权限组管理
     * ********************************/
    //权限管理
    public function group() {
        $fid = in($_GET[0]); //读取用户
        $act = $_GET['action'];
        if ($act == "del") {//删除用户
            if ($fid == 1) {
                echo "超级管理员禁止删除";
                return;
            }
            $condition['id'] = $fid;
            $result = $this->model->table('group')->where($condition)->delete();
            echo 1;
        }else{
            $info = $this->model->table('group')->where()->select();
            $this->assign('info', $info);
            $this->display('manage/group');   
        }
    }
    //编辑数据
    public function groupedit() {
        $action = $_POST['action'];
        $close = in($_GET[close]);
        if($action == 'post'){//更新
            $fid = in($_POST['fid']);
            $name = in($_POST['name']);
            $power_value = in($_POST['qx']);
            if (!$name) {
                Error::show('权限组名称没有填写', 1);
            }
            if (!$power_value) {
                Error::show('管理功能最少选择一项', 1);
            }
            $_POST['power_value'] = implode(',', $power_value);
            unset($_POST['qx']);
            $_POST['update'] = time();
            $data = postinput($_POST);
            if ($fid) {
                if ($fid == 1) {
                    Error::show('超级管理员权限组禁止修改', 1);
                }
                $condition['id'] = $fid;
                $result = $this->model->table('group')->data($data)->where($condition)->update();
            } else {
                $result = $this->model->table('group')->data($data)->insert();
            }
            Error::show('提交已成功操作！载入中....', 0, $this->closewindow());
        }else{
            $fid = in($_GET[0]);
            if ($fid == 1) {
                Error::show('超级管理员权限组禁止修改');
            }
            //读取权限
            $condition['id'] = intval($fid);
            $arrres = $this->model->table('resource')->where('pid = 0')->order('list asc')->select();
            $this->assign('list', $arrres);
            //读取岗位
            $info = $this->model->table('group')->where($condition)->find();
            $info['power_value'] = explode(',', $info['power_value']);
            $this->assign('close', $close);
            $this->assign('info', $info);
            $this->display('manage/groupedit');   
        }
    }
    //获取权限分类
    public function getAction($pid) {
        if (empty($pid))
            return false;
        return $this->model->table('resource')->where('pid=' . $pid)->order('list asc')->select();
    }
    //#####################################################
    //系统等级
    public function site() {
        $info = $this->model->table('site')->where()->select();
        $this->assign('info', $info);
        $this->display('manage/site');
    }
    //编辑等级
    public function siteedit() {
        $action = $_POST['action'];
        $close = in($_GET[close]);
        if ($action == 'post') {
            $fid = $_POST['fid'];
            if (!$_POST['about']) {
                Error::show('简单备注没有填写', 1);
            }
            $condition['id'] = intval($fid);
            $data = postinput($_POST);
            $result = $this->model->table('site')->data($data)->where($condition)->update();
            Error::show('已成功提交！', 0, __URL__ . '/siteedit-'.$fid.'.html');
        }else {
            $fid = in($_GET[0]); //读取用户
            $condition['id'] = intval($fid);
            $info = $this->model->table('site')->where($condition)->find();
            $this->assign('info', $info);
            $this->assign('close', $close);
            $this->display('manage/siteedit');
        }
    }
}