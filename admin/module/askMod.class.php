<?php
class askMod extends commonMod{
    public function __construct(){
        parent::__construct();
    }
    //#####################################################
    //方法:调查列表
    //参数:无
    public function index(){
        $act = $_REQUEST['action'];
        if ($act == "alldel") {//批量删除
            $fid = $_POST['fid'];
            if (is_array($fid)) {
                foreach ($fid as $value) {//开始删除
                    $condition['id'] = in($value);
                    $this->model->table('askclass')->where($condition)->delete();
                    $conclass['classid'] = in($value);
                    $this->model->table('ask')->where($conclass)->delete();
                    $this->model->table('vate')->where($conclass)->delete();
                }
                Error::show('已成功删除！自动返回....', 0, __URL__ . '/index');
            } else {
                Error::show('没有选择任何要删除的资源', 1);
            }
        }elseif($act == "del"){//单条删除
            $fid = in($_GET[0]);
            $condition['id'] = $fid;
            $this->model->table('askclass')->where($condition)->delete();
            $conclass['classid'] = $fid;
            $this->model->table('ask')->where($conclass)->delete();
            echo 1;
        }else{
            $info = $this->model->table('askclass')->where()->select();
            $this->assign('info', $info);
            $this->display('ask/index');   
        }
    }
    //方法:编辑主题
    public function edit(){
        $action = $_POST['action'];
        if ($action == 'post') {
            $fid = $_POST['fid'];
            $msg = Check::rule(
                    array(check::must($_POST['title']), '调查主题必须填写'),
                    array(check::must($_POST['about']), '备注必须填写'),
                    array(check::must($_POST['titlekey']), 'SEO副标题必须填写'),
                    array(check::must($_POST['keywords']), 'SEO关键词必须填写'),
                    array(check::must($_POST['description']), 'SEO页面说明必须填写')
             );
            if ($msg !== true) {
                Error::show($msg, 1);
            }
            $_POST['starttime'] = strtotime($_POST['starttime']);
            $_POST['endtime']   = strtotime($_POST['endtime']);
            $data = postinput($_POST);
            if ($fid) {
                $condition['id'] = $fid;
                $result = $this->model->table('askclass')->data($data)->where($condition)->update();
            } else {
                $result = $this->model->table('askclass')->data($data)->insert();
            }
            Error::show('已成功提交！', 0,$this->closewindow());
        } else {
            $fid = in($_GET[0]); //读取用户
            $condition['id'] = intval($fid);
            $info = $this->model->table('askclass')->where($condition)->find();
            $this->assign('info', $info);
            $this->display('ask/edit');
        }
    }
    /*******************************
     * 调查内容管理
     *******************************/
    //方法:调查查看
    public function asklist(){
        $act = $_REQUEST['action'];
        $fid = in($_GET[0]); //读取用户
        if($act == "del"){//单条删除
            $fid = in($_GET[0]);
            $condition['id'] = $fid;
            $this->model->table('ask')->where($condition)->delete();
            $convate['askid'] = $fid;
            $this->model->table('vate')->where($convate)->delete();
            echo 1;
        }else{
            $condition['id'] = intval($fid);
            $title = $this->model->table('askclass')->field('id,title')->where($condition)->find();
            $this->assign('title', $title);
            $conask['classid'] = intval($fid);
            $info = $this->model->table('ask')->where($conask)->select();
            $this->assign('info', $info);
            $convate['classid'] = intval($fid);
            $vate = $this->model->table('vate')->field('id,classid,askid,name')->where($convate)->select();
            $this->assign('vate', $vate);     
            $this->display('ask/ask');
        }
    }
    //删除问题答案
    public function delvate(){
        $fid   = in($_GET[0]);
        $cid   = in($_GET[1]);
        $condition['id'] = $fid;
        $result = $this->model->table('vate')->where($condition)->delete();
        if($result){
            Error::show('成功删除！', 0, __URL__.'/asklist-'.$cid.'.html');  
        }else{
            Error::show('删除失败', 1);
        }
    }
    //方法:添加调查
    public function askedit(){
        $action = $_POST['action'];
        if ($action == 'post') {
            $fid = in($_POST['fid']);
            $msg = Check::rule(
                    array(check::must($_POST['classid']), '调查主题必须填写'),
                    array(check::must($_POST['name']), '选项名称必须填写')
            );
            if ($msg !== true) {
                Error::show($msg, 1);
            }
            //插入主表
            $ask['radio'] = in($_POST['radio']);
            $ask['name']  = in($_POST['name']);
            if ($fid) {
                $condition['id'] = $fid;
                $this->model->table('ask')->data($ask)->where($condition)->update();
            } else {
                $ask['classid']  = $_POST['classid'];
                $this->model->table('ask')->data($ask)->insert();
                $fid = mysql_insert_id();
            }
            //循环写题库
            $option = $_POST['option'];
            if(is_array($option)){
                $vatedata['askid']   = in($_POST['fid']);
                $vatedata['classid'] = in($_POST['classid']);
                $vatedata['askid']   = $fid;
                foreach ($option as $key =>$value) {
                    $vatedata['name'] = in($value);
                    $voteid = in($_POST['voteid'][$key]);
                    if ($voteid) {
                        $vatecon['id'] = $voteid;
                        $this->model->table('vate')->data($vatedata)->where($vatecon)->update();
                    } else {
                        $this->model->table('vate')->data($vatedata)->insert();
                    }  
                }  
            }
            Error::show('已成功提交！', 0, $this->closewindow());  
        } else {
            $classid = in($_GET[0]); //读取调查专题
            $fid     = in($_GET[1]); //读取调查问题
            $condition['id'] = intval($fid);//问题名称
            $info = $this->model->table('ask')->where($condition)->find();
            $convate['askid'] = intval($fid);//调查问题
            $vate = $this->model->table('vate')->where($convate)->select();
            $this->assign('info', $info);
            $this->assign('vate', $vate);
            $this->assign('classid', $classid);
            $this->display('ask/askedit');
        }
    }
    /* * *****************************
     * 调查结果显示
     * ***************************** */
    //调查结果显示
    public function vate(){
        $info = $this->model->table('askclass')->where()->select();
        $this->assign('info', $info);
        $this->display('ask/vate');   
    }
    //调查结果统计
    public function vatecount(){
        $fid = in($_GET[0]); //读取用户
        $condition['id'] = intval($fid);
        $title = $this->model->table('askclass')->field('id,title')->where($condition)->find();
        $this->assign('title', $title);
        $conask['classid'] = intval($fid);
        $info = $this->model->table('ask')->where($conask)->select();
        $this->assign('info', $info);
        $convate['classid'] = intval($fid);
        $vate = $this->model->table('vate')->field('id,classid,askid,name,vatecount')->where($convate)->select();
        $this->assign('vate', $vate);     
        $this->display('ask/vatecount');
    }
}