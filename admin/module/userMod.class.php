<?php
class userMod extends commonMod {
    public function __construct() {
        parent::__construct();
    }
    public function index() {
        $fid = in($_GET[0]); //读取用户
        $act = $_GET['action'];
        if ($act == "del") {//删除用户
            $conorder = array();
            $conorder['userid'] = intval($fid);;
            $info = $this->model->table('order')->field('userid')->where($conorder)->find(); 
            if($info){
                echo "还有订单数据，禁止删除"; 
            }
            $this->model->table('guestbook')->where($conorder)->delete();
            $this->model->table('comment')->where($conorder)->delete();
            $condition = array();
            $condition['id'] = intval($fid);
            $this->model->table('user')->where($condition)->delete();
            echo 1;
        } else {
            if($_GET['search']){
                $keyword = '&'.$_SERVER['QUERY_STRING'];
                $condition ='username LIKE \'%' . $_GET['keyword'] . '%\'';
                $this->assign('keyword', $_GET['keyword']);
            }
            //分页开始
            $url = __URL__ . '/index-{page}.html'.$keyword;
            $listRows = 20; //每页显示的信息条数 
            $page = new Page();
            $cur_page = $page->getCurPage($url);
            $limit_start = ($cur_page - 1) * $listRows;
            $limit = $limit_start . ',' . $listRows;
            //获取总行数
            $allcount = $this->model->table('user')->where($condition)->count();
            $this->assign('allcount', $allcount);
            $this->assign('page', $page->show($url, $allcount, $listRows, 10, 4));
            //读取数据开始
            $info = $this->model->table('user')->where($condition)->limit($limit)->select();
            $this->assign('info', $info);
            $this->display('user/index');
        }
    }
    //阅读查看
    public function read(){
        $fid = in($_GET[0]); //读取用户
        $condition['id'] = intval($fid);
        $info = $this->model->table('user')->where($condition)->find();
        $this->assign('info', $info);
        $this->display('user/read');
    }
    //群发邮件
    public function allemail(){
        $action = $_POST['action'];
        if ($action == 'postemail') {
            $fid = $_POST['fid'];
            if (is_array($fid)) {
                $this->assign('email', $fid);
            }else{
                Error::show('没有选择要发群发邮件的用户', 1);
            }
            $this->display('user/allemail');
        }elseif ($action == 'post') {
           $emailadd = $_POST['emailadd'];
           $emailtitle = $_POST['emailtitle'];
           $emailcontent = $_POST['emailcontent'];
           $msg = Check::rule(
                   array(check::must($_POST['emailtitle']), '必须填写发信主题'),
                   array(check::must($_POST['emailcontent']), '必须填写发信内容')
                   );
           if ($msg !== true){
               Error::show($msg, 1);
           }
           Email::init($this->config);    //初始化配置 //参数1：接收人的邮箱地址；参数2：邮件标题；参数3：邮件内容
           Email::send($emailadd,$emailtitle,$emailcontent);   //发送邮件 
           Error::show('信箱发送成功', 0, __URL__);
        }
    }
    //管理员编辑
    public function edit() {
        $action = $_POST['action'];
        $close = in($_GET[close]);
        if ($action == 'post') {
            $fid = $_POST['fid'];
            $email = in($_POST['username']);
            $email = in($_POST['email']);
            $iphone = in($_POST['iphone']);
            if ($_POST['password'])
                $_POST['password'] = md5($_POST['password']);
            $_POST['regtime'] = time();
            $data = postinput($_POST);
            if ($fid) {
                $sql = "id <> $fid and (username = '$username' or email = '$email' or iphone ='$iphone')";
                $info = $this->model->table('user')->where($sql)->find();
                if($info){
                    if ($username == $info['username']) {
                        Error::show('相同的' . $username . '已存在！', 1);
                    }
                    if ($email == $info['email']) {
                        Error::show('相同的' . $email . '已存在！', 1);
                    }
                    if ($iphone == $info['iphone']) {
                        Error::show('相同的' . $iphone . '已存在！', 1);
                    }
                    Error::show('有相同信息禁止修改！', 1);
                }
                $condition['id'] = $fid;
                $result = $this->model->table('user')->data($data)->where($condition)->update();
            } else {
                $condition = "username = '$username' or email = '$email' or iphone ='$iphone'";
                $info = $this->model->table('user')->where($condition)->find();
                if ($info) {
                    if ($username == $info['username']) {
                        Error::show('相同的' . $username . '已存在！', 1);
                    }
                    if ($email == $info['email']) {
                        Error::show('相同的' . $email . '已存在！', 1);
                    }
                    if ($iphone == $info['iphone']) {
                        Error::show('相同的' . $iphone . '已存在！', 1);
                    }
                    Error::show('有相同信息禁止修改！', 1);
                }
                $result = $this->model->table('user')->data($data)->insert();
            }
            Error::show('已成功提交！', 0, $this->closewindow());
        } else {
            $fid = in($_GET[0]); //读取用户
            $close = in($_GET[close]);
            $condition['id'] = intval($fid);
            $info = $this->model->table('user')->where($condition)->find();
            $this->assign('info', $info);
            $this->assign('close', $close);
            $this->display('user/edit');
        }
    }
    public function sms(){
        Error::show('短信接口没有开通', 0);
    }
    public function email(){
        $action = $_POST['action'];
        if($action == 'post'){//删除
           $emailadd = $_POST['emailadd'];
           $emailtitle = $_POST['emailtitle'];
           $emailcontent = $_POST['emailcontent'];
           $msg = Check::rule(
                   array(check::must($_POST['emailadd']), '收件人Email地址必须填写'),
                   array(check::must($_POST['emailtitle']), '必须填写发信主题'),
                   array(check::must($_POST['emailcontent']), '必须填写发信内容')
                   );
           if ($msg !== true){
               Error::show($msg, 1);
           }
           Email::init($this->config);    //初始化配置 //参数1：接收人的邮箱地址；参数2：邮件标题；参数3：邮件内容
           Email::send($emailadd,$emailtitle,$emailcontent);   //发送邮件 
           Error::show('信箱发送成功', 0, $this->closewindow());
        }else{
            $email = in($_GET[email]); //读取用户
            $close = in($_GET[close]);
            $this->assign('close', $close);
            $this->assign('email', $email);
            $this->display('user/email'); 
        }
    }
    /*   * *************************************
     * 商品分类方法集
     * ***************************************** */
    //方法:商品订单列表
    //参数:无
    public function order(){
        $act = $_REQUEST['action'];
        if ($act == "alldel") {//批量删除
            $fid = $_POST['fid'];
            if (is_array($fid)) {
                foreach ($fid as $value) {//开始删除
                    $condition['id'] = in($value);
                    $this->model->table('order')->where($condition)->delete();
                }
                Error::show('已成功删除！自动返回....', 0, __URL__ . '/order');
            } else {
                Error::show('没有选择任何要删除的资源', 1);
            }
        }elseif ($act == "del") {
            $fid = in($_GET[0]); 
            $condition['id'] = $fid;
            $result = $this->model->table('order')->where($condition)->delete();
            if($result){
                echo 1;
            }else{
                echo "删除失败";
            }
        } else{
           if($_GET['search']){
                $keyword = '&'.$_SERVER['QUERY_STRING'];
                $starttime = strtotime($_GET['starttime']);
                $endtime   = strtotime($_GET['endtime']);
                $condition ="shoptime>=".$starttime." and shoptime<=".$endtime;
                $this->assign('starttime', $starttime);
                $this->assign('endtime', $endtime);
            }
            //分页开始
            $url = __URL__ . '/order-{page}.html'.$keyword;
            $listRows = 20; //每页显示的信息条数 
            $page = new Page();
            $cur_page = $page->getCurPage($url);
            $limit_start = ($cur_page - 1) * $listRows;
            $limit = $limit_start . ',' . $listRows;
            //获取总行数
            $allcount = $this->model->table('order')->where($condition)->count();
            $this->assign('allcount', $allcount);
            $this->assign('page', $page->show($url, $allcount, $listRows, 10, 4)); 
          //读取数据开始
            $info = $this->model->table('order')->where($condition)->limit($limit)->select();
            $this->assign('info', $info);
            $this->display('user/order');   
        }
    }
    public function orderpass(){
        $fid = in($_GET[0]); //读取用户
        $act = $_GET['action'];
        if ($act == "goto") {
            $condition['id'] = $fid;
            $info = $this->model->table('order')->where($condition)->field('new')->find();
            $data = array();
            if($info['new'] == 1){
               $data['new'] = null; 
            }else{
               $data['new'] = 1; 
            }
            $result = $this->model->table('order')->data($data)->where($condition)->update();
            if($data['new']){
                echo '已发货';
            }else{
                echo '<span class="hot">新单</span>';
            }
        }  
    }
    //阅读查看
    public function orderread(){
        $fid = in($_GET[0]); //读取用户
        $condition['id'] = intval($fid);
        $info = $this->model->table('order')->where($condition)->find();
        $this->assign('info', $info);
        $this->display('user/orderread');
    }
}